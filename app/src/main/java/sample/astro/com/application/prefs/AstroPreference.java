package sample.astro.com.application.prefs;


public interface AstroPreference {

     void setUserLoggedIn(String name);

     boolean isUserLoggedIn();

     String getUserLogin();
}
