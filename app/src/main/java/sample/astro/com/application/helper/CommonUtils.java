package sample.astro.com.application.helper;

import android.support.annotation.NonNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class CommonUtils {

    public static final String DATE_TIME_FORMAT_CALL_API = "yyyy-MM-dd HH:mm";
    public static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String TIME_FORMAT = "HH:mm:ss";
    public static final String TIME_FORMAT_DISPLAY = "h:mm a";

    public static <T> boolean isListValid(List<T> list) {
        return list != null && !list.isEmpty();
    }

    public static Calendar convertToCalendar(String dateTime, String format) {
        SimpleDateFormat fmt = getSimpleDateFormat(format);
        Calendar calendar = Calendar.getInstance();
        try {
            Date date = fmt.parse(dateTime);
            calendar.setTime(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return calendar;
    }

    public static int getMinuteFromDate(String dateTime, String format) {
        Calendar calendar = convertToCalendar(dateTime, format);
        return getTotalMinuteFromCalendar(calendar);
    }

    public static int getTotalMinuteFromCalendar(Calendar calendar) {
        return calendar.get(Calendar.MINUTE) + (calendar.get(Calendar.HOUR_OF_DAY) * 60);
    }

    public static String getDateCallApi(Calendar calendar) {
        return convertCalendarToStringFormat(calendar, DATE_TIME_FORMAT_CALL_API);
    }

    public static String getTimeDisplay(String dateTime) {
        Calendar calendar = convertToCalendar(dateTime, DATE_TIME_FORMAT);
        return convertCalendarToStringFormat(calendar, TIME_FORMAT_DISPLAY);
    }

    @NonNull
    private static SimpleDateFormat getSimpleDateFormat(String format) {
        return new SimpleDateFormat(format);
    }

    @NonNull
    private static String convertCalendarToStringFormat(Calendar calendar, String format) {
        SimpleDateFormat fmt = getSimpleDateFormat(format);
        return fmt.format(calendar.getTime());
    }
}
