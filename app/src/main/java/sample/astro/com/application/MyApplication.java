package sample.astro.com.application;

import android.app.Application;

import com.google.firebase.FirebaseApp;

import io.realm.Realm;
import sample.astro.com.application.injection.AppComponent;
import sample.astro.com.application.injection.DaggerAppComponent;
import sample.astro.com.application.injection.modules.ApiModule;
import sample.astro.com.application.injection.modules.AppModule;

public class MyApplication extends Application {
    private static AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(getApplicationContext());
        FirebaseApp.initializeApp(getApplicationContext());
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .apiModule(new ApiModule(ApiConstants.HOST_URL, ApiConstants.TIMEOUT_IN_MINUTE))
                .build();
        appComponent.realmHelper().init();
    }

    public static AppComponent getAppComponent() {
        return appComponent;
    }
}
