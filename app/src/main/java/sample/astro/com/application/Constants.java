package sample.astro.com.application;

public class Constants {

    public static final String REFERENCE_LOGO = "Logo";
    public static final int GRID_COLUMN = 24;
    public static final int NUMBER_CHANNEL_EACH_PAGE = 10;
    public static final int REQUEST_CODE_LOGIN = 1000;
    public static final int SORT_NAME = 0;
    public static final int SORT_NUMBER = 1;
}
