package sample.astro.com.application;

public class ApiConstants {
    static final String HOST_URL = "http://ams-api.astro.com.my/";
    public static final String SUFFIX_VERSION = "ams/v3/";
    static final int TIMEOUT_IN_MINUTE = 2;

    public class HttpCode {
        public static final int SUCCESS = 200;
        public static final int BAD_REQUEST = 400;
        public static final int UNAUTHORIZE = 401;
        public static final int FORBIDDEN = 403;
        public static final int NOT_FOUND = 404;
        public static final int NOT_ALLOW = 405;
        public static final int SERVER_ERROR = 500;
    }
}
