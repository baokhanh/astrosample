package sample.astro.com.application.injection.modules;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import sample.astro.com.application.MyApplication;
import sample.astro.com.application.helper.RealmHelper;
import sample.astro.com.application.prefs.AstroPreference;
import sample.astro.com.application.prefs.AstroPreferenceImpl;

@Singleton
@Module
public class AppModule {

    private final MyApplication application;

    public AppModule(MyApplication application) {
        this.application = application;
    }

    @Provides
    @Singleton
    Context provideApplicationContext() {
        return application.getApplicationContext();
    }

    @Provides
    @Singleton
    AstroPreference provideAppPreference(AstroPreferenceImpl appPreference) {
        return appPreference;
    }

    @Provides
    @Singleton
    Gson provideGson() {
        return new GsonBuilder().serializeNulls().create();
    }

    @Provides
    @Singleton
    RealmHelper provideRealmHelper() {
        return new RealmHelper();
    }
}
