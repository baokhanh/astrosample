package sample.astro.com.application.prefs;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import javax.inject.Inject;


public class AstroPreferenceImpl implements AstroPreference {
    private static final String PREFS_NAME = "AstroPreferenceImpl";
    private static final String PREFS_USER_LOGGED_IN = "prefs_user_logged_in";

    private final SharedPreferences sharedPreferences;

    @Inject
    public AstroPreferenceImpl(Context context) {
        Context applicationContext = context.getApplicationContext();
        this.sharedPreferences = applicationContext.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    }

    @Override
    public void setUserLoggedIn(String name) {
        setString(PREFS_USER_LOGGED_IN, name);
    }

    @Override
    public boolean isUserLoggedIn() {
        return !TextUtils.isEmpty(getUserLogin());
    }

    @Override
    public String getUserLogin() {
        return getString(PREFS_USER_LOGGED_IN);
    }

    public void remove(String key) {
        sharedPreferences.edit().remove(key).apply();
    }

    private void setBoolean(String key, boolean value) {
        sharedPreferences.edit().putBoolean(key, value).apply();
    }

    private Boolean getBoolean(String key) {
        return sharedPreferences.getBoolean(key, false);
    }

    private void setString(String key, String value) {
        sharedPreferences.edit().putString(key, value).apply();
    }

    private String getString(String key) {
        return sharedPreferences.getString(key, "");
    }
}
