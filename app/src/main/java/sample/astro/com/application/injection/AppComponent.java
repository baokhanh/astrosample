package sample.astro.com.application.injection;

import javax.inject.Singleton;

import dagger.Component;
import okhttp3.OkHttpClient;
import retrofit2.CallAdapter;
import retrofit2.Retrofit;
import sample.astro.com.application.injection.modules.ApiModule;
import sample.astro.com.application.helper.RealmHelper;
import sample.astro.com.application.injection.modules.AppModule;
import sample.astro.com.application.prefs.AstroPreference;

@Singleton
@Component(modules = {AppModule.class,
        ApiModule.class})
public interface AppComponent {

    Retrofit retrofit();

    OkHttpClient okHttpClient();

    CallAdapter.Factory factory();

    AstroPreference appPreference();

    RealmHelper realmHelper();
}

