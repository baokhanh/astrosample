package sample.astro.com.application.helper;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmModel;
import sample.astro.com.features.getchannels.model.ChannelInfo;

public class RealmHelper {
    private static final String REALM_DEFAULT_NAME = "Astro";
    private static final String TAG = "RealmHelper";
    private Realm mRealm;

    public void init() {
        RealmConfiguration config = new RealmConfiguration.Builder()
                .name(REALM_DEFAULT_NAME)
                .schemaVersion(0)
                .deleteRealmIfMigrationNeeded()
                .build();
        try {
            mRealm = Realm.getInstance(config);
        } catch (Exception e) {
            Realm.deleteRealm(config);
            mRealm = Realm.getInstance(config);
        }
    }

    public void addChannelToFavorite(ChannelInfo channelInfo) {
        mRealm.executeTransaction(realm -> realm.copyToRealmOrUpdate(channelInfo));
    }

    public void removeChannelOutOfFavoriteList(ChannelInfo channelInfo) {
        mRealm.executeTransactionAsync(realm -> {
            ChannelInfo channelInfoDb =
                    realm.where(ChannelInfo.class)
                            .equalTo("channelId", channelInfo.channelId)
                            .findFirst();
            if (channelInfoDb != null) {
                channelInfoDb.isFavorite = false;
            }
        });
    }

    public List<ChannelInfo> getFavoriteList() {
        return mRealm.copyFromRealm(
                mRealm.where(ChannelInfo.class)
                        .equalTo("isFavorite", true)
                        .findAll());
    }

    public Set<Integer> getFavoriteListId() {
        List<ChannelInfo> channelInfos = getFavoriteList();
        Set<Integer> channelIds = new HashSet<>();
        if (CommonUtils.isListValid(channelInfos)) {
            for (ChannelInfo channelInfo : channelInfos) {
                channelIds.add(channelInfo.channelId);
            }
        }
        return channelIds;
    }

    public <E extends RealmModel> boolean isHasObject(Class<E> clazz) {
        return !mRealm.where(clazz).findAll().isEmpty();
    }

    public List<ChannelInfo> getChannelList() {
        return mRealm.copyFromRealm(
                mRealm.where(ChannelInfo.class)
                        .findAll());
    }

    public void saveListChannelToDb(List<ChannelInfo> channelInfos) {
        mRealm.executeTransactionAsync(realm -> realm.copyToRealmOrUpdate(channelInfos));
    }
}
