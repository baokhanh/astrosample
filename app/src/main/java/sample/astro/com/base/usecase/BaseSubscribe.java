package sample.astro.com.base.usecase;

import android.support.annotation.NonNull;
import android.util.Log;

import io.reactivex.observers.DisposableObserver;
import sample.astro.com.base.view.BaseView;

public abstract class BaseSubscribe<T> extends DisposableObserver<T> {
    private final boolean isAutoToogleProgress;

    private BaseView view;

    public BaseSubscribe(BaseView view) {
        this(true, view);
    }

    public BaseSubscribe(boolean autoToggleProgress, BaseView view) {
        this.view = view;
        this.isAutoToogleProgress = autoToggleProgress;
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (isAutoToogleProgress) {
            view.showProgressDialog(true);
        }
    }

    @Override
    public void onComplete() {
        if (isAutoToogleProgress) {
            view.showProgressDialog(false);
        }
    }

    @Override
    public void onError(@NonNull Throwable e) {
        Log.e("", "onError: " + e);
        if (isAutoToogleProgress) {
            view.showProgressDialog(false);
        }
    }
}