package sample.astro.com.base.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import sample.astro.com.R;
import sample.astro.com.base.presenter.BasePresenter;
import sample.astro.com.base.view.BaseView;


public abstract class BaseFragment<T extends BasePresenter> extends Fragment implements BaseView {

    @Nullable
    @BindView(R.id.viewLoading)
    View progressBar;

    @NonNull
    protected abstract T getPresenter();

    private Unbinder unBinder;

    @Override
    @CallSuper
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        onInitComponent();
    }

    @Nullable
    @Override
    @CallSuper
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutId(), container, false);
        unBinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    @CallSuper
    public void onDestroy() {
        super.onDestroy();
        if (unBinder != null) {
            unBinder.unbind();
        }
        getPresenter().onDestroy();
    }

    @Override
    public void showProgressDialog(boolean isShow) {
        if (progressBar != null) {
            progressBar.setVisibility(isShow ? View.VISIBLE : View.GONE);
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && getView() != null) {
            onScreenVisible();
        }
    }

    @Override
    public Context getApplicationContext() {
        return getActivity().getApplicationContext();
    }

    protected void onInitComponent() {
        // Stub method
    }

    protected abstract int getLayoutId();

    public void onScreenVisible() {
        // Stub method
    }
}
