package sample.astro.com.base.presenter;

import android.support.annotation.CallSuper;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import sample.astro.com.application.prefs.AstroPreference;
import sample.astro.com.base.view.BaseView;

public class BasePresenterImpl<T extends BaseView> implements BasePresenter {

    protected final CompositeDisposable compositeDisposable = new CompositeDisposable();

    protected final AstroPreference appPreference;
    protected final T view;

    public BasePresenterImpl(T view, AstroPreference appPreference) {
        this.appPreference = appPreference;
        this.view = view;
    }

    @Override
    @CallSuper
    public void onDestroy() {
        // Stub method to override
        compositeDisposable.dispose();
    }

    protected <T> void addToDisposable(DisposableObserver<T> disposableObserver) {
        compositeDisposable.add(disposableObserver);
    }
}
