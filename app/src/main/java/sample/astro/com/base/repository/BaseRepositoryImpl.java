package sample.astro.com.base.repository;

public abstract class BaseRepositoryImpl<T> {

    protected T service;

    public BaseRepositoryImpl(T service) {
        this.service = service;
    }
}
