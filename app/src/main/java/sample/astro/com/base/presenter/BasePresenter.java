package sample.astro.com.base.presenter;

import android.os.Bundle;
import android.view.View;

public interface BasePresenter {
    void onDestroy();
}
