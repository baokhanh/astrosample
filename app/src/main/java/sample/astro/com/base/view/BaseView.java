package sample.astro.com.base.view;

import android.content.Context;

public interface BaseView {
    Context getApplicationContext();
    void showProgressDialog(boolean isShow);
}
