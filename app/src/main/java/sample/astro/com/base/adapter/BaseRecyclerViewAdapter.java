package sample.astro.com.base.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import butterknife.ButterKnife;

public abstract class BaseRecyclerViewAdapter<T> extends RecyclerView.Adapter<BaseRecyclerViewAdapter.GenericViewHolder> {

    protected LayoutInflater inflater;
    protected Context context;
    protected List<T> itemList;

    public BaseRecyclerViewAdapter(Context context, List<T> items) {
        // Cache the LayoutInflate to avoid asking for a new one each time.
        this.inflater = LayoutInflater.from(context);
        this.itemList = items;
        this.context = context;
    }

    @Override
    public GenericViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return onCreateContentHolder(parent);
    }

    @Override
    public void onBindViewHolder(GenericViewHolder holder, int position) {
        holder.bindItem(position, getItem(position));
    }

    @Override
    public int getItemCount() {
        if(itemList != null){
            return itemList.size();
        }
        return 0;
    }

    protected T getItem(int pos) {
        if (pos >= 0 && itemList != null && itemList.size() > pos) {
            return itemList.get(pos);
        }
        return null;
    }

    protected View inflateView(ViewGroup parent, int layoutId) {
        return LayoutInflater.from(context).inflate(layoutId, parent, false);
    }

    public abstract static class GenericViewHolder extends RecyclerView.ViewHolder {
        public GenericViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public abstract void bindItem(int position, Object item);
    }

    public abstract GenericViewHolder onCreateContentHolder(ViewGroup parent);
}