package sample.astro.com.base.model.api;

import com.google.gson.annotations.SerializedName;

public abstract class BaseResponse {

    @SerializedName("ErrorCode")
    private Integer errorCode;

    @SerializedName("ErrorMessage")
    private String errorMessage;

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
