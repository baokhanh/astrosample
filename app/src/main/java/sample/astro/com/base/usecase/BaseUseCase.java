package sample.astro.com.base.usecase;

import android.support.annotation.NonNull;

import io.reactivex.Observable;
import sample.astro.com.application.scheduler.AstroScheduler;
import sample.astro.com.base.model.api.BaseResponse;

public class BaseUseCase {

    protected  <T extends BaseResponse> Observable<T> request(@NonNull Observable<T> rawObservable) {
        return rawObservable
                .observeOn(AstroScheduler.mainThread())
                .subscribeOn(AstroScheduler.io());
    }

}
