package sample.astro.com.features.getchannels.presenter;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import sample.astro.com.application.Constants;
import sample.astro.com.application.helper.RealmHelper;
import sample.astro.com.application.prefs.AstroPreference;
import sample.astro.com.base.presenter.BasePresenterImpl;
import sample.astro.com.features.getchannels.model.ChannelInfo;
import sample.astro.com.features.getchannels.view.BaseChannelView;
import sample.astro.com.features.getinfofirebase.model.Channels;
import sample.astro.com.features.getinfofirebase.model.UserChannelInfo;

public class BaseChannelPresenterImpl<T extends BaseChannelView> extends BasePresenterImpl<T> implements BaseChannelPresenter {
    protected static final String TAG = "BaseChannelPresenter";
    protected RealmHelper realmHelper;
    protected DatabaseReference mFirebaseDatabase;
    protected FirebaseDatabase mFirebaseDatabaseInstance;
    protected int currentSort;

    public BaseChannelPresenterImpl(T view, AstroPreference appPreference, RealmHelper realmHelper) {
        super(view, appPreference);
        this.realmHelper = realmHelper;
        mFirebaseDatabaseInstance = mFirebaseDatabaseInstance.getInstance();
        mFirebaseDatabase = mFirebaseDatabaseInstance.getReference("userChannelInfo");
    }

    @Override
    public void sortChannelNumber(List<ChannelInfo> itemLists) {
        currentSort = Constants.SORT_NUMBER;
        Collections.sort(itemLists, (left, right) -> left.channelStbNumber.compareTo(right.channelStbNumber));
        view.displayListChannel();
    }

    @Override
    public void sortChannelName(List<ChannelInfo> itemLists) {
        currentSort = Constants.SORT_NAME;
        Collections.sort(itemLists, (left, right) -> left.channelTitle.compareTo(right.channelTitle));
        view.displayListChannel();
    }

    @Override
    public void removeChannelOutOfFavoriteList(ChannelInfo info) {
        realmHelper.removeChannelOutOfFavoriteList(info);
    }

    @Override
    public void saveListChannelToDb(List<ChannelInfo> channelInfos) {
        realmHelper.saveListChannelToDb(channelInfos);
    }

    protected void addToFireBase(ChannelInfo info) {
        //get ref to channels
        String channelId = mFirebaseDatabase.push().getKey();
        Channels channels = new Channels(info.channelId, info.channelTitle, Integer.parseInt(info.channelStbNumber), true);
        List<Channels> channelsList = new ArrayList<>();
        channelsList.add(channels);
        UserChannelInfo userChannelInfo = new UserChannelInfo(appPreference.getUserLogin(), channelsList, currentSort);

        mFirebaseDatabase.child(channelId).setValue(userChannelInfo);
    }

}
