package sample.astro.com.features.tvguide.api.model.searchevent;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Events {
    @SerializedName("id")
    private String id;
    @SerializedName("fields")
    private List<Fields> fields;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Fields> getFields() {
        return fields;
    }

    public void setFields(List<Fields> fields) {
        this.fields = fields;
    }
}
