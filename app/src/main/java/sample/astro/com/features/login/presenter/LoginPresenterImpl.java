package sample.astro.com.features.login.presenter;

import android.content.Intent;
import android.util.Log;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

import javax.inject.Inject;

import sample.astro.com.application.prefs.AstroPreference;
import sample.astro.com.base.presenter.BasePresenterImpl;
import sample.astro.com.features.login.view.LoginView;

public class LoginPresenterImpl extends BasePresenterImpl<LoginView> implements LoginPresenter {
    private static final String TAG = "Login";
    private GoogleSignInClient googleSignInClient;

    @Inject
    public LoginPresenterImpl(LoginView view, AstroPreference appPreference) {
        super(view, appPreference);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        googleSignInClient = GoogleSignIn.getClient(view.getApplicationContext(), gso);
    }

    @Override
    public void login() {
        view.goToSignInScreen(googleSignInClient);
    }

    @Override
    public void revokeAccess() {
        googleSignInClient.revokeAccess().addOnCompleteListener(task -> view.onRevokeAccessComplete());
    }

    @Override
    public void handleSignInResult(Intent data) {
        // The Task returned from this call is always completed, no need to attach
        // a listener.
        Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
        try {
            GoogleSignInAccount account = task.getResult(ApiException.class);
            // Signed in successfully, show authenticated UI.
            appPreference.setUserLoggedIn(account.getAccount().name);
            Log.d(TAG, "Name: " + account.getDisplayName());
            view.backToHomeScreen();
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
        }
    }
}
