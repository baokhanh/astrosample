package sample.astro.com.features.tvguide.view.customview;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import java.util.Calendar;
import java.util.List;

import sample.astro.com.R;
import sample.astro.com.application.Constants;
import sample.astro.com.application.helper.CommonUtils;
import sample.astro.com.features.tvguide.model.EventInfo;

public class TvGuideEventGridView extends FrameLayout implements BaseTvGuideCustomView {
    private View viewOnNow;
    private FrameLayout layoutEvent;
    private int widthRow;
    private int heightRow;
    private int sizeLine;
    private float widthEachMinute;
    private Drawable drawableVertical;
    private Drawable drawableHorizontal;
    private int width;

    public TvGuideEventGridView(@NonNull Context context) {
        super(context);
        init(context);
    }

    public TvGuideEventGridView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public TvGuideEventGridView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        int gridColumn = Constants.GRID_COLUMN;
        sizeLine = getResources().getDimensionPixelSize(R.dimen.tv_guide_line_size);
        widthRow = sizeLine + getResources().getDimensionPixelSize(R.dimen.tv_guide_row_width);
        heightRow = sizeLine + getResources().getDimensionPixelSize(R.dimen.tv_guide_row_height);

        width = widthRow * gridColumn;
        widthEachMinute = widthRow / 60f;// each row for 1h = 60

        drawableVertical = ContextCompat.getDrawable(context, R.drawable.channel_list_divider);
        drawableHorizontal = ContextCompat.getDrawable(context, R.drawable.channel_list_divider_horizontal);

        updateMainLayoutParams(0);

        // add layout event
        layoutEvent = new FrameLayout(context);
        LayoutParams mainParam = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        layoutEvent.setLayoutParams(mainParam);
        addView(layoutEvent);

        // view on now
        viewOnNow = new View(context);
        viewOnNow.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.tv_guide_line_on_now));
        updateLayoutOnNow();
        addView(viewOnNow);
    }

    private void drawLineHorizontal(int height, int leftMargin, int topMargin) {
        View lineView = new View(getContext());
        lineView.setBackgroundDrawable(drawableHorizontal);

        LayoutParams params = new LayoutParams(sizeLine, height);
        params.gravity = Gravity.TOP;
        params.leftMargin = leftMargin;
        params.topMargin = topMargin;
        lineView.setLayoutParams(params);

        layoutEvent.addView(lineView);
    }

    private void updateMainLayoutParams(int row) {
        LayoutParams mainParam = new LayoutParams(width, row * heightRow);
        setLayoutParams(mainParam);
    }

    private void drawVerticalLine(int row) {
        View lineView = new View(getContext());
        lineView.setBackgroundDrawable(drawableVertical);

        LayoutParams params = new LayoutParams(width, sizeLine);
        params.gravity = Gravity.TOP;
        params.setMargins(sizeLine, (heightRow * row) - sizeLine, 0, 0);
        lineView.setLayoutParams(params);

        layoutEvent.addView(lineView);
    }

    public void addEvents(int row, List<EventInfo> eventInfos) {
        Log.e("Abc", "add event: " + row);
        drawVerticalLine(row);
        for (EventInfo eventInfo : eventInfos) {
            int posBegin = getPositionBeginOfEvent(eventInfo);
            int widthView = getPositionEndOfEvent(eventInfo, posBegin) - sizeLine;
            TvGuideEventView eventView = new TvGuideEventView(getContext());
            if (isEventOnNow()) {
                eventView.setBackgroundColor(Color.GRAY);
            } else {
                eventView.setBackgroundColor(Color.WHITE);
            }
            eventView.updateInfo(eventInfo);
            int height = heightRow;
            int topMargin = row * heightRow;

            //for last item, sometimes duration > last column -> only display length = last column
            if (posBegin + widthView > width) {
                widthView = width - posBegin;
            }
            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(widthView, height);
            params.leftMargin = posBegin + sizeLine;
            params.topMargin = topMargin + sizeLine / 2;
            eventView.setLayoutParams(params);
            layoutEvent.addView(eventView);

            drawLineHorizontal(height, posBegin, topMargin);
        }
    }

    private boolean isEventOnNow() {
        return false;
    }

    private void updateLayoutOnNow() {
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(sizeLine, ViewGroup.LayoutParams.MATCH_PARENT);
        params.leftMargin = getPositionOnNow();
        params.gravity = Gravity.TOP;
        viewOnNow.setLayoutParams(params);
    }

    private int getPositionOnNow() {
        int totalMinute = CommonUtils.getTotalMinuteFromCalendar(Calendar.getInstance());
        int pos = (int) (totalMinute * widthEachMinute);
        Log.d("Abc", "getPositionOnNow: " + totalMinute + " => " + pos);
        return pos;
    }

    public int getPositionBeginOfEvent(EventInfo eventInfo) {
        int totalMinute = CommonUtils.getMinuteFromDate(eventInfo.displayDateTime, CommonUtils.DATE_TIME_FORMAT);
        int pos = (int) (totalMinute * widthEachMinute);
        Log.d("Abc", "getPositionBeginOfEvent: " + eventInfo.displayDateTime + " : " + totalMinute + " => " + pos);
        return pos;
    }

    public int getPositionEndOfEvent(EventInfo eventInfo, int posBegin) {
        int totalMinute = CommonUtils.getMinuteFromDate(eventInfo.displayDuration, CommonUtils.TIME_FORMAT);
        int pos = ((int) (totalMinute * widthEachMinute)) + posBegin;
        Log.d("Abc", "getPositionEndOfEvent: " + eventInfo.displayDuration + " : " + totalMinute + " => " + pos);
        return pos;
    }

    public void clearGridTvGuide() {
        layoutEvent.removeAllViews();
    }

    public void updateOnNowInfo() {
        // todo
        updateLayoutOnNow();
    }
}
