package sample.astro.com.features.getchannels.api.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import sample.astro.com.base.model.api.BaseResponse;

public class GetChannelListResponse extends BaseResponse {
    @SerializedName("channels")
    private List<Channel> channels;

    public List<Channel> getChannels() {
        return channels;
    }
}

