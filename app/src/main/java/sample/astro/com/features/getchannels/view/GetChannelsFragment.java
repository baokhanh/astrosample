package sample.astro.com.features.getchannels.view;


import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import sample.astro.com.R;
import sample.astro.com.application.Constants;
import sample.astro.com.application.MyApplication;
import sample.astro.com.application.helper.CommonUtils;
import sample.astro.com.base.fragment.BaseFragment;
import sample.astro.com.features.MainActivity;
import sample.astro.com.features.getchannels.injection.DaggerGetChannelsComponent;
import sample.astro.com.features.getchannels.injection.GetChannelsModule;
import sample.astro.com.features.getchannels.model.ChannelInfo;
import sample.astro.com.features.getchannels.presenter.GetChannelsPresenter;
import sample.astro.com.features.getchannels.view.adapter.GetChannelsAdapter;

public class GetChannelsFragment extends BaseFragment<GetChannelsPresenter> implements GetChannelsView, GetChannelsAdapter.OnChannelListClickListener {

    @Inject
    GetChannelsPresenter presenter;
    @BindView(R.id.channel_list)
    RecyclerView rvChannelList;
    private GetChannelsAdapter adapter;
    private List<ChannelInfo> itemLists;

    private DatabaseReference mFirebaseDatabase;
    private FirebaseDatabase mFirebaseDatabaseInstance;
    private static final String TAG = GetChannelsFragment.class.getName();

    public static BaseFragment newInstance() {
        return new GetChannelsFragment();
    }

    @Override
    protected void onInitComponent() {
        super.onInitComponent();
        DaggerGetChannelsComponent.builder()
                .appComponent(MyApplication.getAppComponent())
                .getChannelsModule(new GetChannelsModule(this))
                .build()
                .inject(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.get_channels_fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_channel, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_sort_number:
                getPresenter().sortChannelNumber(itemLists);
                return true;
            case R.id.action_sort_name:
                getPresenter().sortChannelName(itemLists);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @NonNull
    @Override
    protected GetChannelsPresenter getPresenter() {
        return presenter;
    }

    @Override
    public void updateListChannel(List<ChannelInfo> channelInfos) {
        itemLists.clear();
        if (CommonUtils.isListValid(channelInfos)) {
            itemLists.addAll(channelInfos);
        }
    }

    @Override
    public void displayListChannel() {
        if (adapter == null) {
            adapter = new GetChannelsAdapter(getApplicationContext(), itemLists, this);

            DividerItemDecoration itemDecoration = new
                    DividerItemDecoration(getApplicationContext(), DividerItemDecoration.VERTICAL);
            Drawable dividerDrawable = ContextCompat.getDrawable(getApplicationContext(), R.drawable.channel_list_divider);
            itemDecoration.setDrawable(dividerDrawable);
            rvChannelList.addItemDecoration(itemDecoration);

            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext(),
                    LinearLayoutManager.VERTICAL, false);
            rvChannelList.setLayoutManager(linearLayoutManager);
            rvChannelList.setHasFixedSize(true);
            rvChannelList.setAdapter(adapter);
        } else {
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void goToLoginScreen() {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        getActivity().startActivityForResult(intent, Constants.REQUEST_CODE_LOGIN);
    }

    @Override
    public void updateToFireBase() {

    }

    @Override
    public void onAddToFavoriteList(ChannelInfo info) {
        getPresenter().addChannelToFavorite(info);
    }

    @Override
    public void onRemoveOutOfFavoriteList(ChannelInfo info) {
        getPresenter().removeChannelOutOfFavoriteList(info);
    }

    @Override
    public void onScreenVisible() {
        if (itemLists == null && presenter != null) {
            itemLists = new ArrayList<>();
            getPresenter().loadChannelList();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == Constants.REQUEST_CODE_LOGIN ) {
            getPresenter().addChannelToFavoriteAfterLogin();
        }
    }
}
