package sample.astro.com.features.tvguide.api.repository;

import javax.inject.Inject;

import io.reactivex.Observable;
import sample.astro.com.base.repository.BaseRepositoryImpl;
import sample.astro.com.features.tvguide.api.model.getevent.GetEventsResponse;

public class TvGuideRepositoryImpl extends BaseRepositoryImpl<TvGuideApiService> implements TvGuideRepository {
    @Inject
    public TvGuideRepositoryImpl(TvGuideApiService service) {
        super(service);
    }

    @Override
    public Observable<GetEventsResponse> getEvents(String ids, String star, String end) {
        return service.getEvents(ids, star, end);
    }
}
