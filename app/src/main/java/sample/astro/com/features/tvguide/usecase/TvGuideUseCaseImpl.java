package sample.astro.com.features.tvguide.usecase;


import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import sample.astro.com.base.usecase.BaseUseCase;
import sample.astro.com.features.tvguide.api.repository.TvGuideRepository;
import sample.astro.com.features.tvguide.model.TvGuideResult;

public class TvGuideUseCaseImpl extends BaseUseCase implements TvGuideUseCase {

    private final TvGuideRepository repository;

    @Inject
    public TvGuideUseCaseImpl(TvGuideRepository repository) {
        this.repository = repository;
    }

    @Override
    public Observable<TvGuideResult> getEvents(String ids, String star, String end) {
        return repository.getEvents(ids, star, end)
                .map(TvGuideResult::transform)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }

}
