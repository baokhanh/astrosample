package sample.astro.com.features.tvguide.api.model.getevent;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import sample.astro.com.base.model.api.BaseResponse;

public class GetEventsResponse extends BaseResponse {
    @SerializedName("getevent")
    private List<Getevent> getevent;

    public List<Getevent> getGetevent() {
        return getevent;
    }

    public void setGetevent(List<Getevent> getevent) {
        this.getevent = getevent;
    }
}
