package sample.astro.com.features.tvguide.view;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import sample.astro.com.R;
import sample.astro.com.application.MyApplication;
import sample.astro.com.base.fragment.BaseFragment;
import sample.astro.com.features.getchannels.model.ChannelInfo;
import sample.astro.com.features.tvguide.injection.DaggerTvGuideComponent;
import sample.astro.com.features.tvguide.injection.TvGuideModule;
import sample.astro.com.features.tvguide.model.EventInfo;
import sample.astro.com.features.tvguide.presenter.TvGuidePresenter;
import sample.astro.com.features.tvguide.view.customview.TvGuideCustomView;
import sample.astro.com.features.tvguide.view.customview.TvGuideListener;

public class TvGuideFragment extends BaseFragment<TvGuidePresenter> implements TvGuideView, TvGuideListener {

    @Inject
    TvGuidePresenter presenter;
    @BindView(R.id.tv_guide_custom_view)
    TvGuideCustomView tvGuideCustomView;

    public static BaseFragment newInstance() {
        return new TvGuideFragment();
    }

    @Override
    protected void onInitComponent() {
        super.onInitComponent();
        DaggerTvGuideComponent.builder()
                .appComponent(MyApplication.getAppComponent())
                .tvGuideModule(new TvGuideModule(this))
                .build()
                .inject(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.tv_guide_fragment;
    }

    @NonNull
    @Override
    protected TvGuidePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_channel, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_sort_number:
                getPresenter().sortChannelNumber();
                return true;
            case R.id.action_sort_name:
                getPresenter().sortChannelName();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvGuideCustomView.setListener(this);
    }

    @Override
    public void updateListTvGuide(ChannelInfo channelInfo, List<EventInfo> eventInfos) {
        tvGuideCustomView.appendChannelAndEvent(channelInfo, eventInfos);
    }

    @Override
    public void clearGridTvGuide() {
        tvGuideCustomView.clearGridTvGuide();
    }

    @Override
    public void updateOnNowInfo() {
        tvGuideCustomView.updateOnNowInfo();
    }

    @Override
    public void onLoadMore() {
        getPresenter().onLoadMore();
    }

    @Override
    public void onScreenVisible() {
        super.onScreenVisible();
        if(presenter != null) {
            presenter.loadInfoTvGuideShow();
        }
    }
}
