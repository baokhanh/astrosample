package sample.astro.com.features.tvguide.view.customview;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.util.ArrayList;

import sample.astro.com.R;

public class TvGuideTimeView extends FrameLayout {
    private static final int NUMBER_HOUR = 25; // add more 1 hour for next day

    public TvGuideTimeView(Context context) {
        super(context);
        init(context);
    }

    public TvGuideTimeView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public TvGuideTimeView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        // todo update list later
        ArrayList<String> listContents = new ArrayList<>();
        for (int i = 0; i < NUMBER_HOUR; i++) {
            if (i == 0 || i == NUMBER_HOUR - 1) {
                listContents.add("12AM");
            } else if (i < 12) {
                listContents.add(String.format("%dAM", i));
            } else if (i == 12) {
                listContents.add("12PM");
            } else {
                listContents.add(String.format("%dPM", (i % 12)));
            }
        }

        int sizeLine = getResources().getDimensionPixelSize(R.dimen.tv_guide_line_size);
        int widthRow = getResources().getDimensionPixelSize(R.dimen.tv_guide_row_width);
        Drawable drawableHorizontal = ContextCompat.getDrawable(context, R.drawable.channel_list_divider_horizontal);
        int heightRow = sizeLine + getResources().getDimensionPixelSize(R.dimen.tv_guide_time_view_height);

        for (int i = 0; i < listContents.size(); i++) {
            View lineView = new View(context);
            lineView.setBackgroundDrawable(drawableHorizontal);

            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(sizeLine, heightRow / 3);
            params.gravity = Gravity.BOTTOM;
            params.leftMargin = i * widthRow;
            lineView.setLayoutParams(params);
            addView(lineView);

            TextView textView = new TextView(context);
            textView.setText(listContents.get(i));
            FrameLayout.LayoutParams p = new FrameLayout.LayoutParams(widthRow, LayoutParams.WRAP_CONTENT);
            p.leftMargin = i * widthRow;
            textView.setLayoutParams(p);
            addView(textView);
        }
    }
}
