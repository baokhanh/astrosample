package sample.astro.com.features.getchannels.view;

import java.util.List;

import sample.astro.com.features.getchannels.model.ChannelInfo;

public interface GetChannelsView extends BaseChannelView {
    void updateListChannel(List<ChannelInfo> channelInfos);

    void displayListChannel();

    void goToLoginScreen();

    void updateToFireBase();
}
