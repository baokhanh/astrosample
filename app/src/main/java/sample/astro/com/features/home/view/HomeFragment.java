package sample.astro.com.features.home.view;


import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import sample.astro.com.R;
import sample.astro.com.application.MyApplication;
import sample.astro.com.application.helper.CommonUtils;
import sample.astro.com.base.fragment.BaseFragment;
import sample.astro.com.features.getchannels.model.ChannelInfo;
import sample.astro.com.features.home.injection.DaggerHomeComponent;
import sample.astro.com.features.home.injection.HomeModule;
import sample.astro.com.features.home.presenter.HomePresenter;

public class HomeFragment extends BaseFragment<HomePresenter> implements HomeView {

    private static final String TAG = "Home";
    @Inject
    HomePresenter presenter;
    @BindView(R.id.home_list)
    RecyclerView rvHomeList;
    private HomeAdapter adapter;
    private List<ChannelInfo> itemLists = new ArrayList<>();

    public static BaseFragment newInstance() {
        return new HomeFragment();
    }

    @Override
    protected void onInitComponent() {
        super.onInitComponent();
        DaggerHomeComponent.builder()
                .appComponent(MyApplication.getAppComponent())
                .homeModule(new HomeModule(this))
                .build()
                .inject(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.home_fragment;
    }

    @NonNull
    @Override
    protected HomePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(presenter.isUserLoggedIn());
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_home, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_logout:
                presenter.logout();
                return true;
            case R.id.action_sort_number:
                getPresenter().sortChannelNumber(itemLists);
                return true;
            case R.id.action_sort_name:
                getPresenter().sortChannelName(itemLists);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void updateListChannel(List<ChannelInfo> channelInfos) {
        itemLists.clear();
        if (CommonUtils.isListValid(channelInfos)) {
            itemLists.addAll(channelInfos);
        }
    }

    @Override
    public void displayListChannel() {
        if (adapter == null) {
            adapter = new HomeAdapter(getApplicationContext(), itemLists);

            DividerItemDecoration itemDecoration = new
                    DividerItemDecoration(getApplicationContext(), DividerItemDecoration.VERTICAL);
            Drawable dividerDrawable = ContextCompat.getDrawable(getApplicationContext(), R.drawable.channel_list_divider);
            itemDecoration.setDrawable(dividerDrawable);
            rvHomeList.addItemDecoration(itemDecoration);

            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext(),
                    LinearLayoutManager.VERTICAL, false);
            rvHomeList.setLayoutManager(linearLayoutManager);
            rvHomeList.setHasFixedSize(true);
            rvHomeList.setAdapter(adapter);
        } else {
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onScreenVisible() {
        getPresenter().loadListChannelFavorite();
    }

    @Override
    public void onLogoutComplete() {
        Log.d(TAG, "onLogoutComplete");
        setHasOptionsMenu(false);
        getActivity().invalidateOptionsMenu();
        itemLists.clear();
        displayListChannel();
    }
}
