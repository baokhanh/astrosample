package sample.astro.com.features.tvguide.view.customview;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import sample.astro.com.R;
import sample.astro.com.application.Constants;
import sample.astro.com.features.getchannels.model.ChannelInfo;
import sample.astro.com.features.tvguide.model.EventInfo;
import sample.astro.com.features.tvguide.view.TvGuideChannelAdapter;

import static android.view.MotionEvent.*;

public class TvGuideCustomView extends FrameLayout implements TvGuideEventHorizontalScrollView.HorizontalScrollViewListener, TvGuideEventScrollView.ScrollViewListener {
    @BindView(R.id.tv_guide_channel_list)
    RecyclerView rvChannelList;
    @BindView(R.id.tv_guide_time_view)
    TvGuideTimeView tvGuideTimeView;
    @BindView(R.id.tv_guide_event_scroll_view)
    TvGuideEventScrollView scrollView;
    @BindView(R.id.tv_guide_event_horizontal_scroll_view)
    TvGuideEventHorizontalScrollView horizontalScrollView;
    @BindView(R.id.tv_guide_event_view)
    TvGuideEventGridView eventGridView;
    private Context context;
    private TvGuideChannelAdapter adapter;
    private List<ChannelInfo> itemLists;
    private Unbinder unBinder;
    private LinearLayoutManager linearLayoutManager;
    private int firstVisibleItem;
    private int visibleItemCount;
    private int totalItemCount;
    private TvGuideListener listener;

    public TvGuideCustomView(@NonNull Context context) {
        super(context);
        init(context);
    }

    public TvGuideCustomView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public TvGuideCustomView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        this.context = context;
        View view = LayoutInflater.from(getContext()).inflate(
                R.layout.tv_guide_custom_view_main, this);
        unBinder = ButterKnife.bind(this, view);

        scrollView.setScrollViewListener(this);
        horizontalScrollView.setScrollViewListener(this);
        itemLists = new ArrayList<>();
        initChannelList();
        rvChannelList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
//                Log.d("TvGuide", "check state: " + rvChannelList.getScrollState()
//                 + " : " + scrollView.is);
//                scrollView.scrollBy(dx, dy);

                // handle load more
                visibleItemCount = recyclerView.getChildCount();
                totalItemCount = linearLayoutManager.getItemCount();
                firstVisibleItem = linearLayoutManager.findFirstVisibleItemPosition();
                final int lastItem = firstVisibleItem + visibleItemCount;
                if (lastItem == totalItemCount && totalItemCount != 0
                        && lastItem >= Constants.NUMBER_CHANNEL_EACH_PAGE) {
                    // check last item. if scroll to end of list, we will start
                    // load more.
                    View v = recyclerView.getChildAt(visibleItemCount - 1);
                    if (v == null || v.getBottom() > recyclerView.getBottom()) {
                        return;
                    }
                    onLoadMore();
                }
            }
        });
    }

    private void onLoadMore() {
        if (listener != null) {
            listener.onLoadMore();
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (unBinder != null) {
            unBinder.unbind();
        }
    }

    @Override
    public void onScrollChanged(TvGuideEventScrollView scrollView, int x, int y, int oldx, int oldy) {
        rvChannelList.scrollBy(x, y);
    }

    @Override
    public void onHorizontalScrollChanged(TvGuideEventHorizontalScrollView horizontalScrollView, int x, int y, int oldx, int oldy) {
        tvGuideTimeView.scrollTo(x, y);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case ACTION_MOVE:
            case ACTION_POINTER_2_DOWN:
            case ACTION_POINTER_1_DOWN:
            case ACTION_POINTER_1_UP:
            case ACTION_POINTER_2_UP:
            case ACTION_UP:
                return true;
            default:
                return false;
        }
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        return event.getAction() == ACTION_POINTER_2_DOWN;
    }

    public void appendChannelAndEvent(ChannelInfo info, List<EventInfo> eventInfos) {
        itemLists.add(info);
        int pos = itemLists.size() - 1;
        adapter.notifyItemInserted(pos);
        eventGridView.addEvents(pos, eventInfos);
    }

    private void initChannelList() {
        adapter = new TvGuideChannelAdapter(context, itemLists);
        DividerItemDecoration itemDecoration = new
                DividerItemDecoration(context, DividerItemDecoration.VERTICAL);
        Drawable dividerDrawable = ContextCompat.getDrawable(context, R.drawable.channel_list_divider);
        itemDecoration.setDrawable(dividerDrawable);
        rvChannelList.addItemDecoration(itemDecoration);

        linearLayoutManager = new LinearLayoutManager(context,
                LinearLayoutManager.VERTICAL, false);
        rvChannelList.setLayoutManager(linearLayoutManager);
        rvChannelList.setHasFixedSize(true);
        rvChannelList.setAdapter(adapter);
    }

    public void clearGridTvGuide() {
        itemLists.clear();
        adapter.notifyDataSetChanged();
        eventGridView.clearGridTvGuide();
    }

    public void setListener(TvGuideListener listener) {
        this.listener = listener;
    }

    public void updateOnNowInfo() {
        eventGridView.updateOnNowInfo();
    }
}
