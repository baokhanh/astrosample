package sample.astro.com.features.tvguide.view;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import sample.astro.com.R;
import sample.astro.com.base.adapter.BaseRecyclerViewAdapter;
import sample.astro.com.features.getchannels.model.ChannelInfo;
import sample.astro.com.features.getchannels.view.adapter.GetChannelsAdapter;
import sample.astro.com.features.getchannels.view.adapter.GetChannelsViewHolder;

public class TvGuideChannelAdapter extends BaseRecyclerViewAdapter<ChannelInfo> {

    public TvGuideChannelAdapter(Context context, List<ChannelInfo> items) {
        super(context, items);
    }

    @Override
    public GenericViewHolder onCreateContentHolder(ViewGroup parent) {
        View view = inflateView(parent, R.layout.tv_guide_channel_item);
        return new TvGuideChannelViewHolder(context, view, null);
    }

    class TvGuideChannelViewHolder extends GetChannelsViewHolder {

        TvGuideChannelViewHolder(Context context,
                                 View view,
                                 GetChannelsAdapter.OnChannelListClickListener listener) {
            super(context, view, listener);
        }

        @Override
        protected void setChannelName(ChannelInfo channelInfo) {
            tvName.setText(String.format("CH %s", channelInfo.channelStbNumber));
        }
    }
}
