package sample.astro.com.features.login.injection;

import dagger.Module;
import dagger.Provides;
import sample.astro.com.application.injection.quanlifier.PerFragment;
import sample.astro.com.features.login.presenter.LoginPresenter;
import sample.astro.com.features.login.presenter.LoginPresenterImpl;
import sample.astro.com.features.login.view.LoginView;

@Module
@PerFragment
public class LoginModule {
    private LoginView view;

    public LoginModule(LoginView view) {
        this.view = view;
    }

    @Provides
    @PerFragment
    LoginView provideView(){
        return view;

    }

    @Provides
    @PerFragment
    LoginPresenter providePresenter(LoginPresenterImpl presenter){
        return presenter;
    }
}
