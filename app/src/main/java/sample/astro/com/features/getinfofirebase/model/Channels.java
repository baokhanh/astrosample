package sample.astro.com.features.getinfofirebase.model;

import com.google.gson.annotations.SerializedName;

public class Channels {
    @SerializedName("channelId")
    private int channelId;
    @SerializedName("channelTitle")
    private String channelTitle;
    @SerializedName("channelNumber")
    private int channelNumber;
    @SerializedName("favorites")
    private boolean favorites;

    public Channels() {
    }

    public Channels(int channelId, String channelTitle, int channelNumber, boolean favorites) {
        this.channelId = channelId;
        this.channelTitle = channelTitle;
        this.channelNumber = channelNumber;
        this.favorites = favorites;
    }

    public int getChannelId() {
        return channelId;
    }

    public void setChannelId(int channelId) {
        this.channelId = channelId;
    }

    public String getChannelTitle() {
        return channelTitle;
    }

    public void setChannelTitle(String channelTitle) {
        this.channelTitle = channelTitle;
    }

    public int getChannelNumber() {
        return channelNumber;
    }

    public void setChannelNumber(int channelNumber) {
        this.channelNumber = channelNumber;
    }

    public boolean getFavorites() {
        return favorites;
    }

    public void setFavorites(boolean favorites) {
        this.favorites = favorites;
    }
}
