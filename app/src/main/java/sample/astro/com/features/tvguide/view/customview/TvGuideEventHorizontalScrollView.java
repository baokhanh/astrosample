package sample.astro.com.features.tvguide.view.customview;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.HorizontalScrollView;

public class TvGuideEventHorizontalScrollView extends HorizontalScrollView {
    private HorizontalScrollViewListener horizontalScrollViewListener = null;

    public TvGuideEventHorizontalScrollView(Context context) {
        super(context);
    }

    public TvGuideEventHorizontalScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TvGuideEventHorizontalScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onScrollChanged(int x, int y, int oldx, int oldy) {
        super.onScrollChanged(x, y, oldx, oldy);
        if (horizontalScrollViewListener != null) {
            horizontalScrollViewListener.onHorizontalScrollChanged(this, x, y, oldx, oldy);
        }
    }

    public void setScrollViewListener(HorizontalScrollViewListener horizontalScrollViewListener) {
        this.horizontalScrollViewListener = horizontalScrollViewListener;
    }

    public interface HorizontalScrollViewListener {
        void onHorizontalScrollChanged(TvGuideEventHorizontalScrollView horizontalScrollView, int x, int y, int oldx, int oldy);
    }
}
