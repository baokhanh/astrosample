package sample.astro.com.features.home.view;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import sample.astro.com.R;
import sample.astro.com.base.adapter.BaseRecyclerViewAdapter;
import sample.astro.com.features.getchannels.model.ChannelInfo;
import sample.astro.com.features.getchannels.view.adapter.GetChannelsAdapter;
import sample.astro.com.features.getchannels.view.adapter.GetChannelsViewHolder;

public class HomeAdapter extends BaseRecyclerViewAdapter<ChannelInfo> {
    public HomeAdapter(Context context, List<ChannelInfo> items) {
        super(context, items);
    }

    @Override
    public GenericViewHolder onCreateContentHolder(ViewGroup parent) {
        View view = inflateView(parent, R.layout.get_channel_item);
        return new HomeViewHolder(context, view, null);
    }

    class HomeViewHolder extends GetChannelsViewHolder {

        HomeViewHolder(Context context,
                       View view,
                       GetChannelsAdapter.OnChannelListClickListener listener) {
            super(context, view, listener);
        }

        @Override
        public void onClickedFavoriteIcon(View view) {
            // do nothing for this
        }
    }
}
