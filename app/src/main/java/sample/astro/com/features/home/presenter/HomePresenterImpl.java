package sample.astro.com.features.home.presenter;

import android.util.Log;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import sample.astro.com.application.Constants;
import sample.astro.com.application.helper.CommonUtils;
import sample.astro.com.application.helper.RealmHelper;
import sample.astro.com.application.prefs.AstroPreference;
import sample.astro.com.features.getchannels.model.ChannelInfo;
import sample.astro.com.features.getchannels.presenter.BaseChannelPresenterImpl;
import sample.astro.com.features.getinfofirebase.model.Channels;
import sample.astro.com.features.getinfofirebase.model.UserChannelInfo;
import sample.astro.com.features.home.view.HomeView;

public class HomePresenterImpl extends BaseChannelPresenterImpl<HomeView> implements HomePresenter {
    private GoogleSignInClient googleSignInClient;

    @Inject
    public HomePresenterImpl(HomeView view, AstroPreference astroPreference, RealmHelper realmHelper) {
        super(view, astroPreference, realmHelper);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        googleSignInClient = GoogleSignIn.getClient(view.getApplicationContext(), gso);
    }

    @Override
    public void loadListChannelFavorite() {
        if (isUserLoggedIn()) {
            String channelId = mFirebaseDatabase.push().getKey();
            mFirebaseDatabase.child(channelId).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    UserChannelInfo userChannelInfo = dataSnapshot.getValue(UserChannelInfo.class);
                    dataSnapshot.getKey();

                    if (userChannelInfo != null) {
                        List<Channels> channels = userChannelInfo.getChannels();
                        if (CommonUtils.isListValid(channels)) {
                            List<ChannelInfo> channelInfos = new ArrayList<>();
                            for (Channels chanel : channels) {
                                ChannelInfo info = new ChannelInfo();
                                info.channelId = chanel.getChannelId();
                                info.channelTitle = chanel.getChannelTitle();
                                info.channelStbNumber = String.valueOf(chanel.getChannelNumber());
                                info.isFavorite = chanel.getFavorites();
                                channelInfos.add(info);
                            }
                            if (userChannelInfo.getSort() == Constants.SORT_NAME) {
                                sortChannelName(channelInfos);
                            } else {
                                sortChannelNumber(channelInfos);
                            }
                            view.updateListChannel(channelInfos);
                            view.displayListChannel();
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.w(TAG, "Failed to read value.", databaseError.toException());
                }
            });
        }
    }

    @Override
    public boolean isUserLoggedIn() {
        return appPreference.isUserLoggedIn();
    }

    @Override
    public void logout() {
        googleSignInClient.signOut().addOnCompleteListener(task -> {
            appPreference.setUserLoggedIn("");
            view.onLogoutComplete();
        });
    }
}