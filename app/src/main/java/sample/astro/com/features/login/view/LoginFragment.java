package sample.astro.com.features.login.view;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.common.SignInButton;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import sample.astro.com.R;
import sample.astro.com.application.MyApplication;
import sample.astro.com.base.fragment.BaseFragment;
import sample.astro.com.features.login.injection.DaggerLoginComponent;
import sample.astro.com.features.login.injection.LoginModule;
import sample.astro.com.features.login.presenter.LoginPresenter;

public class LoginFragment extends BaseFragment<LoginPresenter> implements LoginView {

    private static final int RC_SIGN_IN = 9001;
    private static final String TAG = LoginFragment.class.getSimpleName();

    @Inject
    LoginPresenter presenter;

    @BindView(R.id.btn_sign_in_google)
    SignInButton mSignInButtonGoogle;

    public static BaseFragment newInstance() {
        return new LoginFragment();
    }

    @Override
    protected void onInitComponent() {
        super.onInitComponent();
        DaggerLoginComponent.builder()
                .appComponent(MyApplication.getAppComponent())
                .loginModule(new LoginModule(this))
                .build()
                .inject(this);
    }

    @NonNull
    @Override
    protected LoginPresenter getPresenter() {
        return presenter;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.login_fragment;
    }

    @OnClick(R.id.btn_sign_in_google)
    void loginWithGoogle() {
        getPresenter().login();
    }

    @OnClick(R.id.btn_revoke_access)
    void revokeAccessGoogle(){
        getPresenter().revokeAccess();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            presenter.handleSignInResult(data);
        }
    }

    @Override
    public void goToSignInScreen(GoogleSignInClient mGoogleSignInClient) {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onRevokeAccessComplete() {
        Log.d(TAG, "onRevokeAccessComplete");
    }

    @Override
    public void backToHomeScreen() {
        Log.d(TAG, "backToHomeScreen");
        getActivity().setResult(Activity.RESULT_OK, getActivity().getIntent());
    }
}
