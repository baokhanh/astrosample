package sample.astro.com.features.tvguide.view.customview;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ScrollView;

public class TvGuideEventScrollView extends ScrollView {
    private ScrollViewListener scrollViewListener = null;

    public TvGuideEventScrollView(Context context) {
        super(context);
    }

    public TvGuideEventScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TvGuideEventScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onScrollChanged(int x, int y, int oldx, int oldy) {
        super.onScrollChanged(x, y, oldx, oldy);
        if(scrollViewListener != null) {
            scrollViewListener.onScrollChanged(this, x, y, oldx, oldy);
        }
    }

    public void setScrollViewListener(ScrollViewListener scrollViewListener) {
        this.scrollViewListener = scrollViewListener;
    }

    public interface ScrollViewListener {
        void onScrollChanged(TvGuideEventScrollView scrollView, int x, int y, int oldx, int oldy);
    }
}
