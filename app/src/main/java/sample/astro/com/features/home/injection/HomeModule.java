package sample.astro.com.features.home.injection;


import dagger.Module;
import dagger.Provides;
import sample.astro.com.application.injection.quanlifier.PerFragment;
import sample.astro.com.features.home.presenter.HomePresenter;
import sample.astro.com.features.home.presenter.HomePresenterImpl;
import sample.astro.com.features.home.view.HomeView;

@Module
@PerFragment
public class HomeModule {
    private HomeView view;

    public HomeModule(HomeView view) {
        this.view = view;
    }

    @Provides
    @PerFragment
    HomeView provideView() {
        return view;
    }

    @Provides
    @PerFragment
    HomePresenter providePresenter(HomePresenterImpl presenter) {
        return presenter;
    }
}
