package sample.astro.com.features.home.presenter;

import sample.astro.com.features.getchannels.presenter.BaseChannelPresenter;

public interface HomePresenter extends BaseChannelPresenter {
    void loadListChannelFavorite();

    boolean isUserLoggedIn();

    void logout();
}
