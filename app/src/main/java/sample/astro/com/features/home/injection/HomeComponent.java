package sample.astro.com.features.home.injection;


import dagger.Component;
import sample.astro.com.application.injection.AppComponent;
import sample.astro.com.application.injection.quanlifier.PerFragment;
import sample.astro.com.features.home.view.HomeFragment;

@PerFragment
@Component(dependencies = AppComponent.class, modules = {HomeModule.class})
public interface HomeComponent {
    void inject(HomeFragment fragment);
}
