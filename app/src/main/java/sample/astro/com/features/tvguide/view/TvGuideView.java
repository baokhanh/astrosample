package sample.astro.com.features.tvguide.view;

import java.util.List;

import sample.astro.com.base.view.BaseView;
import sample.astro.com.features.getchannels.model.ChannelInfo;
import sample.astro.com.features.tvguide.model.EventInfo;

public interface TvGuideView extends BaseView {

    void updateListTvGuide(ChannelInfo channelInfo, List<EventInfo> eventInfos);

    void clearGridTvGuide();

    void updateOnNowInfo();
}
