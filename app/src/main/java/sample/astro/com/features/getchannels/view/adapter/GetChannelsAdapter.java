package sample.astro.com.features.getchannels.view.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import sample.astro.com.R;
import sample.astro.com.base.adapter.BaseRecyclerViewAdapter;
import sample.astro.com.features.getchannels.model.ChannelInfo;

public class GetChannelsAdapter extends BaseRecyclerViewAdapter<ChannelInfo> {
    private OnChannelListClickListener listener;
    public GetChannelsAdapter(Context context, List<ChannelInfo> items, OnChannelListClickListener listener) {
        super(context, items);
        this.listener = listener;
    }

    @Override
    public GenericViewHolder onCreateContentHolder(ViewGroup parent) {
        View view = inflateView(parent, R.layout.get_channel_item);
        return new GetChannelsViewHolder(context, view, listener);
    }

    public interface OnChannelListClickListener {
        void onAddToFavoriteList(ChannelInfo info);
        void onRemoveOutOfFavoriteList(ChannelInfo info);
    }
}
