package sample.astro.com.features.getinfofirebase.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserChannelInfo {

    @SerializedName("user")
    @Expose
    private String user;
    @SerializedName("channels")
    @Expose
    private List<Channels> channels = null;
    @SerializedName("sort")
    @Expose
    private int sort;

    public UserChannelInfo(String user, List<Channels> channels, int sort) {
        this.user = user;
        this.channels = channels;
        this.sort = sort;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public List<Channels> getChannels() {
        return channels;
    }

    public void setChannels(List<Channels> channels) {
        this.channels = channels;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }
}
