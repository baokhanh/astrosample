package sample.astro.com.features.login.injection;

import dagger.Component;
import sample.astro.com.application.injection.AppComponent;
import sample.astro.com.application.injection.quanlifier.PerFragment;
import sample.astro.com.features.login.view.LoginFragment;

@PerFragment
@Component(dependencies = AppComponent.class, modules = LoginModule.class)
public interface LoginComponent {
    void inject(LoginFragment fragment);
}
