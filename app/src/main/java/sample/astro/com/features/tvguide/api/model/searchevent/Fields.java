package sample.astro.com.features.tvguide.api.model.searchevent;

import com.google.gson.annotations.SerializedName;

public class Fields {
    @SerializedName("siTrafficKey")
    private String siTrafficKey;
    @SerializedName("channelTitle")
    private String channelTitle;
    @SerializedName("channelHD")
    private boolean channelHD;
    @SerializedName("channelStbNumber")
    private String channelStbNumber;
    @SerializedName("channelId")
    private String channelId;
    @SerializedName("contentId")
    private int contentId;
    @SerializedName("programKey")
    private String programKey;
    @SerializedName("episodeKey")
    private String episodeKey;
    @SerializedName("stbProgrammeKey")
    private String stbProgrammeKey;
    @SerializedName("stbEpisodeKey")
    private String stbEpisodeKey;
    @SerializedName("displayDateTime")
    private String displayDateTime;
    @SerializedName("displayEndDateTime")
    private String displayEndDateTime;
    @SerializedName("displayLanguage")
    private String displayLanguage;
    @SerializedName("programmeTitle")
    private String eventName;
    @SerializedName("eventDescription")
    private String eventDescription;
    @SerializedName("eventExtendedDescription")
    private String eventExtendedDescription;
    @SerializedName("genre")
    private String genre;
    @SerializedName("subGenre")
    private String subGenre;
    @SerializedName("errorMessage")
    private String errorMessage;
    @SerializedName("isError")
    private String isError;

    public String getSiTrafficKey() {
        return siTrafficKey;
    }

    public void setSiTrafficKey(String siTrafficKey) {
        this.siTrafficKey = siTrafficKey;
    }

    public String getChannelTitle() {
        return channelTitle;
    }

    public void setChannelTitle(String channelTitle) {
        this.channelTitle = channelTitle;
    }

    public boolean getChannelHD() {
        return channelHD;
    }

    public void setChannelHD(boolean channelHD) {
        this.channelHD = channelHD;
    }

    public String getChannelStbNumber() {
        return channelStbNumber;
    }

    public void setChannelStbNumber(String channelStbNumber) {
        this.channelStbNumber = channelStbNumber;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public int getContentId() {
        return contentId;
    }

    public void setContentId(int contentId) {
        this.contentId = contentId;
    }

    public String getProgramKey() {
        return programKey;
    }

    public void setProgramKey(String programKey) {
        this.programKey = programKey;
    }

    public String getEpisodeKey() {
        return episodeKey;
    }

    public void setEpisodeKey(String episodeKey) {
        this.episodeKey = episodeKey;
    }

    public String getStbProgrammeKey() {
        return stbProgrammeKey;
    }

    public void setStbProgrammeKey(String stbProgrammeKey) {
        this.stbProgrammeKey = stbProgrammeKey;
    }

    public String getStbEpisodeKey() {
        return stbEpisodeKey;
    }

    public void setStbEpisodeKey(String stbEpisodeKey) {
        this.stbEpisodeKey = stbEpisodeKey;
    }

    public String getDisplayDateTime() {
        return displayDateTime;
    }

    public void setDisplayDateTime(String displayDateTime) {
        this.displayDateTime = displayDateTime;
    }

    public String getDisplayEndDateTime() {
        return displayEndDateTime;
    }

    public void setDisplayEndDateTime(String displayEndDateTime) {
        this.displayEndDateTime = displayEndDateTime;
    }

    public String getDisplayLanguage() {
        return displayLanguage;
    }

    public void setDisplayLanguage(String displayLanguage) {
        this.displayLanguage = displayLanguage;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventDescription() {
        return eventDescription;
    }

    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription;
    }

    public String getEventExtendedDescription() {
        return eventExtendedDescription;
    }

    public void setEventExtendedDescription(String eventExtendedDescription) {
        this.eventExtendedDescription = eventExtendedDescription;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getSubGenre() {
        return subGenre;
    }

    public void setSubGenre(String subGenre) {
        this.subGenre = subGenre;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getIsError() {
        return isError;
    }

    public void setIsError(String isError) {
        this.isError = isError;
    }
}
