package sample.astro.com.features.tvguide.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import sample.astro.com.base.model.BaseUiInfo;
import sample.astro.com.features.tvguide.api.model.getevent.GetEventsResponse;
import sample.astro.com.features.tvguide.api.model.getevent.Getevent;

public class TvGuideResult extends BaseUiInfo implements Serializable {
    public List<EventInfo> eventInfos;

    public static TvGuideResult transform(GetEventsResponse response) {
        TvGuideResult model = new TvGuideResult();
        model.errorCode = response.getErrorCode();
        model.errorMessage = response.getErrorMessage();
        List<EventInfo> eventInfos = new ArrayList<>();
        for(Getevent info: response.getGetevent()){
            eventInfos.add(EventInfo.transform(info));
        }
        model.eventInfos = eventInfos;
        return model;
    }
}
