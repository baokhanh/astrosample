package sample.astro.com.features.getchannels.view.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import javax.annotation.Nullable;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.Optional;
import sample.astro.com.R;
import sample.astro.com.base.adapter.BaseRecyclerViewAdapter;
import sample.astro.com.features.getchannels.model.ChannelInfo;

public class GetChannelsViewHolder extends BaseRecyclerViewAdapter.GenericViewHolder {
    @Nullable
    @BindView(R.id.channel_item_tv_number)
    protected TextView tvNumber;
    @BindView(R.id.channel_item_tv_name)
    protected TextView tvName;
    @Nullable
    @BindView(R.id.channel_item_iv_add_favorite)
    protected ImageView ivAddFavorite;
    @BindView(R.id.channel_item_logo)
    protected ImageView ivLogo;
    private GetChannelsAdapter.OnChannelListClickListener listener;
    private ChannelInfo channelInfo;
    private Context context;

    public GetChannelsViewHolder(Context context, View view, GetChannelsAdapter.OnChannelListClickListener listener) {
        super(view);
        this.context = context;
        this.listener = listener;
    }

    @Override
    public void bindItem(int position, Object item) {
        channelInfo = (ChannelInfo) item;
        if(tvNumber != null) {
            tvNumber.setText(String.valueOf(channelInfo.channelStbNumber));
        }
        setChannelName(channelInfo);
        if(ivAddFavorite != null) {
            ivAddFavorite.setSelected(channelInfo.isFavorite);
        }
        if(!TextUtils.isEmpty(channelInfo.imageLogo)) {
            Glide.with(context).load(channelInfo.imageLogo)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(ivLogo);
        }
    }

    protected void setChannelName(ChannelInfo channelInfo) {
        tvName.setText(channelInfo.channelTitle);
    }

    @Optional
    @OnClick(R.id.channel_item_iv_add_favorite)
    public void onClickedFavoriteIcon(View view) {
        channelInfo.isFavorite = !channelInfo.isFavorite;
        view.setSelected(channelInfo.isFavorite);
        if(channelInfo.isFavorite) {
            listener.onAddToFavoriteList(channelInfo);
        }else {
            listener.onRemoveOutOfFavoriteList(channelInfo);
        }
    }
}
