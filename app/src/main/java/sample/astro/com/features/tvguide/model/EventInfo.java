package sample.astro.com.features.tvguide.model;

import sample.astro.com.features.tvguide.api.model.getevent.Getevent;

public class EventInfo {
    public String eventID;
    public int channelId;
    public String channelStbNumber;
    public String displayDateTime;
    public String displayDuration;
    public String programmeTitle;

    public static EventInfo transform(Getevent response) {
        EventInfo model = new EventInfo();
        model.eventID = response.getEventID();
        model.channelId = response.getChannelId();
        model.channelStbNumber = response.getChannelStbNumber();
        model.displayDateTime = response.getDisplayDateTime();
        model.displayDuration = response.getDisplayDuration();
        model.programmeTitle = response.getProgrammeTitle();
        return model;
    }
}
