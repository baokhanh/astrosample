package sample.astro.com.features.tvguide.presenter;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.observers.DisposableObserver;
import sample.astro.com.application.Constants;
import sample.astro.com.application.helper.CommonUtils;
import sample.astro.com.application.helper.RealmHelper;
import sample.astro.com.application.prefs.AstroPreference;
import sample.astro.com.base.presenter.BasePresenterImpl;
import sample.astro.com.base.usecase.BaseSubscribe;
import sample.astro.com.features.getchannels.model.ChannelInfo;
import sample.astro.com.features.tvguide.model.EventInfo;
import sample.astro.com.features.tvguide.model.TvGuideResult;
import sample.astro.com.features.tvguide.usecase.TvGuideUseCase;
import sample.astro.com.features.tvguide.view.TvGuideView;

public class TvGuidePresenterImpl extends BasePresenterImpl<TvGuideView> implements TvGuidePresenter {

    private final RealmHelper realmHelper;
    private final TvGuideUseCase useCase;
    private List<ChannelInfo> originChannelList;
    private List<ChannelInfo> currentChannelList;
    private Map<Integer, List<EventInfo>> mapEvents;
    private int currentPage;
    private int currentChannelLoad;

    @Inject
    public TvGuidePresenterImpl(TvGuideView view, AstroPreference astroPreference, RealmHelper realmHelper, TvGuideUseCase useCase) {
        super(view, astroPreference);
        this.realmHelper = realmHelper;
        this.useCase = useCase;
    }

    @Override
    public void loadInfoTvGuideShow() {
        if (originChannelList == null) {
            originChannelList = new ArrayList<>();
            currentChannelList = new ArrayList<>();
            mapEvents = new HashMap<>();

            // get list channel from db
            if (realmHelper.isHasObject(ChannelInfo.class)) {
                view.showProgressDialog(true);
                List<ChannelInfo> channels = realmHelper.getChannelList();
                if (CommonUtils.isListValid(channels)) {
                    originChannelList.addAll(channels);
                    currentPage = 1;
                    currentChannelLoad = 0;
                    updateListChannel();
                    loadEventList();
                }
            }
        } else {
            view.updateOnNowInfo();
        }
    }

    @Override
    public void updateListChannel() {
        if (currentPage > 2) {
            // for now only support 2 page
            // will improve performance later.
            return;
        }
        int size = currentPage * Constants.NUMBER_CHANNEL_EACH_PAGE;
        if (originChannelList.size() < size) {
            size = originChannelList.size();
        }
        for (int i = currentChannelLoad; i < size; i++) {
            currentChannelList.add(originChannelList.get(i));
        }
    }

    @Override
    public void loadEventList() {
        if (currentChannelLoad < currentChannelList.size()) {
            ChannelInfo channelInfo = currentChannelList.get(currentChannelLoad);
            DisposableObserver<TvGuideResult> disposableObserver = new BaseSubscribe<TvGuideResult>(false, view) {
                @Override
                public void onNext(TvGuideResult result) {
                    List<EventInfo> eventInfos = result.eventInfos;
                    view.updateListTvGuide(channelInfo, eventInfos);
                    if (!mapEvents.containsKey(channelInfo.channelId)) {
                        mapEvents.put(channelInfo.channelId, eventInfos);
                    }
                    currentChannelLoad++;
                    loadEventList();
                }

                @Override
                public void onError(@NonNull Throwable e) {
                    super.onError(e);
                    view.showProgressDialog(false);
                }
            };
            addToDisposable(disposableObserver);

            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.MILLISECOND, 0);

            String star = CommonUtils.getDateCallApi(calendar);
            calendar.add(Calendar.DAY_OF_YEAR, 1);
            String end = CommonUtils.getDateCallApi(calendar);
            useCase.getEvents(String.valueOf(channelInfo.channelId), star, end).subscribe(disposableObserver);
        } else {
            view.showProgressDialog(false);
        }
    }

    @Override
    public void onLoadMore() {
        view.showProgressDialog(true);
        currentPage++;
        updateListChannel();
        loadEventList();
    }

    @Override
    public void sortChannelNumber() {
        Collections.sort(currentChannelList, (left, right) -> left.channelStbNumber.compareTo(right.channelStbNumber));
        updateTvGuide();
    }

    @Override
    public void sortChannelName() {
        Collections.sort(currentChannelList, (left, right) -> left.channelTitle.compareTo(right.channelTitle));
        updateTvGuide();
    }

    private void updateTvGuide() {
        view.clearGridTvGuide();
        for (ChannelInfo info : currentChannelList) {
            view.updateListTvGuide(info, mapEvents.get(info.channelId));
        }
    }
}