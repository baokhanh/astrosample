package sample.astro.com.features.tvguide.api.model.getevent;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Getevent {
    @SerializedName("eventID")
    private String eventID;
    @SerializedName("channelId")
    private int channelId;
    @SerializedName("channelStbNumber")
    private String channelStbNumber;
    @SerializedName("channelHD")
    private String channelHD;
    @SerializedName("channelTitle")
    private String channelTitle;
    @SerializedName("epgEventImage")
    private String epgEventImage;
    @SerializedName("certification")
    private String certification;
    @SerializedName("displayDateTimeUtc")
    private String displayDateTimeUtc;
    @SerializedName("displayDateTime")
    private String displayDateTime;
    @SerializedName("displayDuration")
    private String displayDuration;
    @SerializedName("siTrafficKey")
    private String siTrafficKey;
    @SerializedName("programmeTitle")
    private String programmeTitle;
    @SerializedName("programmeId")
    private String programmeId;
    @SerializedName("episodeId")
    private String episodeId;
    @SerializedName("shortSynopsis")
    private String shortSynopsis;
    @SerializedName("longSynopsis")
    private String longSynopsis;
    @SerializedName("actors")
    private String actors;
    @SerializedName("directors")
    private String directors;
    @SerializedName("producers")
    private String producers;
    @SerializedName("genre")
    private String genre;
    @SerializedName("subGenre")
    private String subGenre;
    @SerializedName("live")
    private boolean live;
    @SerializedName("premier")
    private boolean premier;
    @SerializedName("ottBlackout")
    private boolean ottBlackout;
    @SerializedName("highlight")
    private String highlight;
    @SerializedName("contentId")
    private int contentId;
    @SerializedName("contentImage")
    private List<ContentImage> contentImage;
    @SerializedName("groupKey")
    private int groupKey;
    @SerializedName("vernacularData")
    private List<VernacularData> vernacularData;

    public String getEventID() {
        return eventID;
    }

    public void setEventID(String eventID) {
        this.eventID = eventID;
    }

    public int getChannelId() {
        return channelId;
    }

    public void setChannelId(int channelId) {
        this.channelId = channelId;
    }

    public String getChannelStbNumber() {
        return channelStbNumber;
    }

    public void setChannelStbNumber(String channelStbNumber) {
        this.channelStbNumber = channelStbNumber;
    }

    public String getChannelHD() {
        return channelHD;
    }

    public void setChannelHD(String channelHD) {
        this.channelHD = channelHD;
    }

    public String getChannelTitle() {
        return channelTitle;
    }

    public void setChannelTitle(String channelTitle) {
        this.channelTitle = channelTitle;
    }

    public String getEpgEventImage() {
        return epgEventImage;
    }

    public void setEpgEventImage(String epgEventImage) {
        this.epgEventImage = epgEventImage;
    }

    public String getCertification() {
        return certification;
    }

    public void setCertification(String certification) {
        this.certification = certification;
    }

    public String getDisplayDateTimeUtc() {
        return displayDateTimeUtc;
    }

    public void setDisplayDateTimeUtc(String displayDateTimeUtc) {
        this.displayDateTimeUtc = displayDateTimeUtc;
    }

    public String getDisplayDateTime() {
        return displayDateTime;
    }

    public void setDisplayDateTime(String displayDateTime) {
        this.displayDateTime = displayDateTime;
    }

    public String getDisplayDuration() {
        return displayDuration;
    }

    public void setDisplayDuration(String displayDuration) {
        this.displayDuration = displayDuration;
    }

    public String getSiTrafficKey() {
        return siTrafficKey;
    }

    public void setSiTrafficKey(String siTrafficKey) {
        this.siTrafficKey = siTrafficKey;
    }

    public String getProgrammeTitle() {
        return programmeTitle;
    }

    public void setProgrammeTitle(String programmeTitle) {
        this.programmeTitle = programmeTitle;
    }

    public String getProgrammeId() {
        return programmeId;
    }

    public void setProgrammeId(String programmeId) {
        this.programmeId = programmeId;
    }

    public String getEpisodeId() {
        return episodeId;
    }

    public void setEpisodeId(String episodeId) {
        this.episodeId = episodeId;
    }

    public String getShortSynopsis() {
        return shortSynopsis;
    }

    public void setShortSynopsis(String shortSynopsis) {
        this.shortSynopsis = shortSynopsis;
    }

    public String getLongSynopsis() {
        return longSynopsis;
    }

    public void setLongSynopsis(String longSynopsis) {
        this.longSynopsis = longSynopsis;
    }

    public String getActors() {
        return actors;
    }

    public void setActors(String actors) {
        this.actors = actors;
    }

    public String getDirectors() {
        return directors;
    }

    public void setDirectors(String directors) {
        this.directors = directors;
    }

    public String getProducers() {
        return producers;
    }

    public void setProducers(String producers) {
        this.producers = producers;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getSubGenre() {
        return subGenre;
    }

    public void setSubGenre(String subGenre) {
        this.subGenre = subGenre;
    }

    public boolean getLive() {
        return live;
    }

    public void setLive(boolean live) {
        this.live = live;
    }

    public boolean getPremier() {
        return premier;
    }

    public void setPremier(boolean premier) {
        this.premier = premier;
    }

    public boolean getOttBlackout() {
        return ottBlackout;
    }

    public void setOttBlackout(boolean ottBlackout) {
        this.ottBlackout = ottBlackout;
    }

    public String getHighlight() {
        return highlight;
    }

    public void setHighlight(String highlight) {
        this.highlight = highlight;
    }

    public int getContentId() {
        return contentId;
    }

    public void setContentId(int contentId) {
        this.contentId = contentId;
    }

    public List<ContentImage> getContentImage() {
        return contentImage;
    }

    public void setContentImage(List<ContentImage> contentImage) {
        this.contentImage = contentImage;
    }

    public int getGroupKey() {
        return groupKey;
    }

    public void setGroupKey(int groupKey) {
        this.groupKey = groupKey;
    }

    public List<VernacularData> getVernacularData() {
        return vernacularData;
    }

    public void setVernacularData(List<VernacularData> vernacularData) {
        this.vernacularData = vernacularData;
    }
}
