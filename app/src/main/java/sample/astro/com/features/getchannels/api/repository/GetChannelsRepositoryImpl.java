package sample.astro.com.features.getchannels.api.repository;

import javax.inject.Inject;

import io.reactivex.Observable;
import sample.astro.com.base.repository.BaseRepositoryImpl;
import sample.astro.com.features.getchannels.api.model.GetChannelListResponse;
import sample.astro.com.features.getchannels.api.model.GetChannelsResponse;

public class GetChannelsRepositoryImpl extends BaseRepositoryImpl<GetChannelsApiService> implements GetChannelsRepository {
    @Inject
    public GetChannelsRepositoryImpl(GetChannelsApiService service) {
        super(service);
    }

    @Override
    public Observable<GetChannelListResponse> getChannelList() {
        return service.getChannelList();
    }

    @Override
    public Observable<GetChannelsResponse> getChannels() {
        return service.getChannels();
    }
}
