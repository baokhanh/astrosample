package sample.astro.com.features.tvguide.view.customview;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Html;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import sample.astro.com.R;
import sample.astro.com.application.helper.CommonUtils;
import sample.astro.com.features.tvguide.model.EventInfo;

public class TvGuideEventView extends FrameLayout {
    @BindView(R.id.event_item_name)
    TextView tvName;
    private Unbinder unBinder;

    public TvGuideEventView(@NonNull Context context) {
        super(context);
        init(context);
    }

    public TvGuideEventView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public TvGuideEventView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        View view = LayoutInflater.from(context).inflate(
                R.layout.tv_guide_event_item, this);
        unBinder = ButterKnife.bind(this, view);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (unBinder != null) {
            unBinder.unbind();
        }
    }

    public void updateInfo(EventInfo eventInfo){
        tvName.setText(Html.fromHtml(String.format(getContext().getString(R.string.event_info),
                eventInfo.programmeTitle,
                CommonUtils.getTimeDisplay(eventInfo.displayDateTime))));
    }
}
