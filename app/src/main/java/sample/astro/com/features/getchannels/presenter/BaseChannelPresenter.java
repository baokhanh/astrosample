package sample.astro.com.features.getchannels.presenter;

import java.util.List;

import sample.astro.com.base.presenter.BasePresenter;
import sample.astro.com.features.getchannels.model.ChannelInfo;

public interface BaseChannelPresenter extends BasePresenter {

    void sortChannelNumber(List<ChannelInfo> itemLists);

    void sortChannelName(List<ChannelInfo> itemLists);

    void removeChannelOutOfFavoriteList(ChannelInfo info);

    void saveListChannelToDb(List<ChannelInfo> channelInfos);
}
