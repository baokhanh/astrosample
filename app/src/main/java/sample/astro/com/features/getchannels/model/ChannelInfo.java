package sample.astro.com.features.getchannels.model;

import java.util.List;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import sample.astro.com.application.Constants;
import sample.astro.com.application.helper.CommonUtils;
import sample.astro.com.features.getchannels.api.model.Channel;
import sample.astro.com.features.getchannels.api.model.ChannelExtRef;

public class ChannelInfo extends RealmObject {
    @PrimaryKey
    public int channelId;
    public String channelTitle;
    public String channelStbNumber;
    public String imageLogo;
    public boolean isFavorite;

    public static ChannelInfo transform(Channel response, boolean isFavorite) {
        ChannelInfo model = new ChannelInfo();
        model.channelId = response.getChannelId();
        model.channelTitle = response.getChannelTitle();
        model.channelStbNumber = response.getChannelStbNumber();
        model.isFavorite = isFavorite;

        // update info logo
        List<ChannelExtRef> channelExtRef = response.getChannelExtRef();
        if(CommonUtils.isListValid(channelExtRef)){
            for(ChannelExtRef ref : channelExtRef){
                if(Constants.REFERENCE_LOGO.equalsIgnoreCase(ref.getSystem())){
                    model.imageLogo = ref.getValue();
                    break;
                }
            }
        }
        return model;
    }
}
