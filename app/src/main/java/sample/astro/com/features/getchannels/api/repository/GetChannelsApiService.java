package sample.astro.com.features.getchannels.api.repository;

import io.reactivex.Observable;
import retrofit2.http.GET;
import sample.astro.com.application.ApiConstants;
import sample.astro.com.features.getchannels.api.model.GetChannelListResponse;
import sample.astro.com.features.getchannels.api.model.GetChannelsResponse;

public interface GetChannelsApiService {
    @GET(ApiConstants.SUFFIX_VERSION + "getChannelList")
    Observable<GetChannelListResponse> getChannelList();

    @GET(ApiConstants.SUFFIX_VERSION + "getChannels")
    Observable<GetChannelsResponse> getChannels();
}
