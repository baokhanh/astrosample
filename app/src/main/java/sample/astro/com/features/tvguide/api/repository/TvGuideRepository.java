package sample.astro.com.features.tvguide.api.repository;

import io.reactivex.Observable;
import sample.astro.com.features.tvguide.api.model.getevent.GetEventsResponse;

public interface TvGuideRepository {
    Observable<GetEventsResponse> getEvents(String ids, String star, String end);
}
