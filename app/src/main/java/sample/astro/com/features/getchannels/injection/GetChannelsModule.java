package sample.astro.com.features.getchannels.injection;


import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import sample.astro.com.application.injection.quanlifier.PerFragment;
import sample.astro.com.features.getchannels.presenter.GetChannelsPresenterImpl;
import sample.astro.com.features.getchannels.api.repository.GetChannelsApiService;
import sample.astro.com.features.getchannels.api.repository.GetChannelsRepository;
import sample.astro.com.features.getchannels.api.repository.GetChannelsRepositoryImpl;
import sample.astro.com.features.getchannels.presenter.GetChannelsPresenter;
import sample.astro.com.features.getchannels.usecase.GetChannelsUseCase;
import sample.astro.com.features.getchannels.usecase.GetChannelsUseCaseImpl;
import sample.astro.com.features.getchannels.view.GetChannelsView;

@Module
@PerFragment
public class GetChannelsModule {
    private GetChannelsView view;

    public GetChannelsModule(GetChannelsView view) {
        this.view = view;
    }

    @Provides
    @PerFragment
    GetChannelsView provideView() {
        return view;
    }

    @Provides
    @PerFragment
    GetChannelsPresenter providePresenter(GetChannelsPresenterImpl presenter) {
        return presenter;
    }

    @Provides
    @PerFragment
    public GetChannelsRepository provideGetChannelsRepository(GetChannelsRepositoryImpl repository) {
        return repository;
    }

    @Provides
    @PerFragment
    public GetChannelsApiService provideGetChannelsApiService(Retrofit retrofit) {
        return retrofit.create(GetChannelsApiService.class);
    }

    @Provides
    @PerFragment
    public GetChannelsUseCase provideGetChannelsUseCase(GetChannelsUseCaseImpl useCase) {
        return useCase;
    }
}
