package sample.astro.com.features.getchannels.usecase;

import io.reactivex.Observable;
import sample.astro.com.features.getchannels.model.GetChannelListResult;
import sample.astro.com.features.getchannels.model.GetChannelsResult;

public interface GetChannelsUseCase {
    Observable<GetChannelListResult> getChannelList();

    Observable<GetChannelsResult> getChannels();
}
