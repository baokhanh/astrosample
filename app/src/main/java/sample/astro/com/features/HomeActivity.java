package sample.astro.com.features;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import sample.astro.com.R;
import sample.astro.com.application.Constants;
import sample.astro.com.base.activity.BaseActivity;
import sample.astro.com.base.fragment.BaseFragment;
import sample.astro.com.base.view.NoSwipeViewPager;
import sample.astro.com.features.getchannels.view.GetChannelsFragment;
import sample.astro.com.features.home.view.HomeFragment;
import sample.astro.com.features.tvguide.view.TvGuideFragment;

public class HomeActivity extends BaseActivity {
    private static final String TAG = "HomeActivity";
    @BindView(R.id.tabs)
    TabLayout tabLayout;
    @BindView(R.id.pager)
    NoSwipeViewPager pager;
    private PagerAdapter adapter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_home;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new PagerAdapter(getSupportFragmentManager());
        adapter.initList(getListFragment());
        pager.setOffscreenPageLimit(3);
        pager.setAdapter(adapter);
        tabLayout.setupWithViewPager(pager);

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                // do nothing
            }

            @Override
            public void onPageSelected(int position) {
                Fragment fragment = adapter.getItem(position);
                if (fragment != null && fragment instanceof BaseFragment) {
                    ((BaseFragment) fragment).onScreenVisible();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                // do nothing
            }
        });
    }

    private List<TabPagerItem> getListFragment() {
        return Arrays.asList(
                new TabPagerItem(HomeFragment.newInstance(),
                        getString(R.string.home)),
                new TabPagerItem(TvGuideFragment.newInstance(),
                        getString(R.string.tv_guide)),
                new TabPagerItem(GetChannelsFragment.newInstance(),
                        getString(R.string.channel)));
    }

    private class PagerAdapter extends FragmentStatePagerAdapter {
        private final List<TabPagerItem> tabPagerItems = new ArrayList<>();

        PagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        void initList(List<TabPagerItem> fragmentList) {
            this.tabPagerItems.addAll(fragmentList);
        }

        @Override
        public Fragment getItem(int position) {
            return tabPagerItems.get(position).fragment;
        }

        @Override
        public int getCount() {
            return tabPagerItems.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabPagerItems.get(position).title;
        }
    }

    private static class TabPagerItem {
        BaseFragment fragment;
        String title;

        TabPagerItem(BaseFragment fragment, String title) {
            this.fragment = fragment;
            this.title = title;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult: " + resultCode + " : " + requestCode);
        if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_CODE_LOGIN && adapter != null && pager != null) {
            Fragment fragment = adapter.getItem(pager.getCurrentItem());
            if (fragment != null) {
                fragment.onActivityResult(requestCode, resultCode, data);
            }
        }
    }
}
