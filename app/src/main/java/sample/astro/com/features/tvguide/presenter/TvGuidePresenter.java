package sample.astro.com.features.tvguide.presenter;

import sample.astro.com.base.presenter.BasePresenter;

public interface TvGuidePresenter extends BasePresenter {
    void loadInfoTvGuideShow();

    void updateListChannel();

    void loadEventList();

    void onLoadMore();

    void sortChannelNumber();

    void sortChannelName();
}
