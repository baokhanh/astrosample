package sample.astro.com.features.getchannels.view;

import java.util.List;

import sample.astro.com.base.view.BaseView;
import sample.astro.com.features.getchannels.model.ChannelInfo;

public interface BaseChannelView extends BaseView {
    void updateListChannel(List<ChannelInfo> channelInfos);

    void displayListChannel();
}
