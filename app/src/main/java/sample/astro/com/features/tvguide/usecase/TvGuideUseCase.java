package sample.astro.com.features.tvguide.usecase;

import sample.astro.com.features.tvguide.model.TvGuideResult;

import io.reactivex.Observable;

public interface TvGuideUseCase {
    Observable<TvGuideResult> getEvents(String ids, String star, String end);
}
