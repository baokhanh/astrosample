package sample.astro.com.features.tvguide.api.repository;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;
import sample.astro.com.application.ApiConstants;
import sample.astro.com.features.tvguide.api.model.getevent.GetEventsResponse;

public interface TvGuideApiService {
    @GET(ApiConstants.SUFFIX_VERSION + "getEvents")
    Observable<GetEventsResponse> getEvents(@Query("channelId") String ids,
                                            @Query("periodStart") String star,
                                            @Query("periodEnd") String end);
}
