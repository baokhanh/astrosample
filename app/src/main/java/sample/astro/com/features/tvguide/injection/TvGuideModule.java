package sample.astro.com.features.tvguide.injection;


import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import sample.astro.com.application.injection.quanlifier.PerFragment;
import sample.astro.com.features.tvguide.presenter.TvGuidePresenter;
import sample.astro.com.features.tvguide.presenter.TvGuidePresenterImpl;
import sample.astro.com.features.tvguide.api.repository.TvGuideApiService;
import sample.astro.com.features.tvguide.api.repository.TvGuideRepository;
import sample.astro.com.features.tvguide.api.repository.TvGuideRepositoryImpl;
import sample.astro.com.features.tvguide.usecase.TvGuideUseCase;
import sample.astro.com.features.tvguide.usecase.TvGuideUseCaseImpl;
import sample.astro.com.features.tvguide.view.TvGuideView;

@Module
@PerFragment
public class TvGuideModule {
    private TvGuideView view;

    public TvGuideModule(TvGuideView view) {
        this.view = view;
    }

    @Provides
    @PerFragment
    TvGuideView provideView() {
        return view;
    }

    @Provides
    @PerFragment
    TvGuidePresenter provideSignUpPresenter(TvGuidePresenterImpl presenter) {
        return presenter;
    }

    @Provides
    @PerFragment
    public TvGuideRepository provideTvGuideRepository(TvGuideRepositoryImpl repository) {
        return repository;
    }

    @Provides
    @PerFragment
    public TvGuideApiService provideTvGuideApiService(Retrofit retrofit) {
        return retrofit.create(TvGuideApiService.class);
    }

    @Provides
    @PerFragment
    public TvGuideUseCase provideTvGuideUseCase(TvGuideUseCaseImpl useCase) {
        return useCase;
    }
}
