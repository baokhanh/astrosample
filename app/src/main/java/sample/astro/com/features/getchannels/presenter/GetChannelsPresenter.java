package sample.astro.com.features.getchannels.presenter;

import sample.astro.com.features.getchannels.model.ChannelInfo;

public interface GetChannelsPresenter extends BaseChannelPresenter {
    void loadChannelList();

    void getChannelListFromServer();

    void getChannelsFromServer();

    void goToLoginScreen();

    void addChannelToFavoriteAfterLogin();

    void addChannelToFavorite(ChannelInfo info);
}
