package sample.astro.com.features.home.view;

import sample.astro.com.features.getchannels.view.BaseChannelView;

public interface HomeView extends BaseChannelView {
    void onLogoutComplete();
}
