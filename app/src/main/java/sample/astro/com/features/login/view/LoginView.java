package sample.astro.com.features.login.view;

import com.google.android.gms.auth.api.signin.GoogleSignInClient;

import sample.astro.com.base.view.BaseView;

public interface LoginView extends BaseView {
    void goToSignInScreen(GoogleSignInClient mGoogleSignInClient);

    void onRevokeAccessComplete();

    void backToHomeScreen();
}
