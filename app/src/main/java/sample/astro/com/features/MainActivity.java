package sample.astro.com.features;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import sample.astro.com.R;
import sample.astro.com.base.activity.BaseActivity;
import sample.astro.com.features.login.view.LoginFragment;

public class MainActivity extends BaseActivity {
    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        replaceFragment(LoginFragment.newInstance(), R.id.container);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
           setResult(RESULT_OK);
           finish();
        }
    }
}
