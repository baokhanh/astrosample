package sample.astro.com.features.tvguide.injection;


import dagger.Component;
import sample.astro.com.application.injection.AppComponent;
import sample.astro.com.application.injection.quanlifier.PerFragment;
import sample.astro.com.features.tvguide.view.TvGuideFragment;

@PerFragment
@Component(dependencies = AppComponent.class, modules = {TvGuideModule.class})
public interface TvGuideComponent {
    void inject(TvGuideFragment fragment);
}
