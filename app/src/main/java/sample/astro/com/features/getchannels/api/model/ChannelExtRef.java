package sample.astro.com.features.getchannels.api.model;

import com.google.gson.annotations.SerializedName;

public class ChannelExtRef {
    @SerializedName("system")
    private String system;
    @SerializedName("subSystem")
    private String subSystem;
    @SerializedName("value")
    private String value;

    public String getSystem() {
        return system;
    }

    public void setSystem(String system) {
        this.system = system;
    }

    public String getSubSystem() {
        return subSystem;
    }

    public void setSubSystem(String subSystem) {
        this.subSystem = subSystem;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
