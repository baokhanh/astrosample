package sample.astro.com.features.login.presenter;

import android.content.Intent;

import sample.astro.com.base.presenter.BasePresenter;

public interface LoginPresenter extends BasePresenter {
    void login();
    void revokeAccess();

    void handleSignInResult(Intent data);
}
