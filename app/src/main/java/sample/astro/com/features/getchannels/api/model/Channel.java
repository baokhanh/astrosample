package sample.astro.com.features.getchannels.api.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Channel {
    @SerializedName("channelId")
    private int channelId;
    @SerializedName("siChannelId")
    private String siChannelId;
    @SerializedName("channelTitle")
    private String channelTitle;
    @SerializedName("channelDescription")
    private String channelDescription;
    @SerializedName("channelLanguage")
    private String channelLanguage;
    @SerializedName("channelColor1")
    private String channelColor1;
    @SerializedName("channelColor2")
    private String channelColor2;
    @SerializedName("channelColor3")
    private String channelColor3;
    @SerializedName("channelCategory")
    private String channelCategory;
    @SerializedName("channelStbNumber")
    private String channelStbNumber;
    @SerializedName("channelHD")
    private boolean channelHD;
    @SerializedName("hdSimulcastChannel")
    private int hdSimulcastChannel;
    @SerializedName("channelStartDate")
    private String channelStartDate;
    @SerializedName("channelEndDate")
    private String channelEndDate;
    @SerializedName("channelExtRef")
    private List<ChannelExtRef> channelExtRef;
    @SerializedName("linearOttMapping")
    private List<LinearOttMapping> linearOttMapping;

    public int getChannelId() {
        return channelId;
    }

    public void setChannelId(int channelId) {
        this.channelId = channelId;
    }

    public String getSiChannelId() {
        return siChannelId;
    }

    public void setSiChannelId(String siChannelId) {
        this.siChannelId = siChannelId;
    }

    public String getChannelTitle() {
        return channelTitle;
    }

    public void setChannelTitle(String channelTitle) {
        this.channelTitle = channelTitle;
    }

    public String getChannelDescription() {
        return channelDescription;
    }

    public void setChannelDescription(String channelDescription) {
        this.channelDescription = channelDescription;
    }

    public String getChannelLanguage() {
        return channelLanguage;
    }

    public void setChannelLanguage(String channelLanguage) {
        this.channelLanguage = channelLanguage;
    }

    public String getChannelColor1() {
        return channelColor1;
    }

    public void setChannelColor1(String channelColor1) {
        this.channelColor1 = channelColor1;
    }

    public String getChannelColor2() {
        return channelColor2;
    }

    public void setChannelColor2(String channelColor2) {
        this.channelColor2 = channelColor2;
    }

    public String getChannelColor3() {
        return channelColor3;
    }

    public void setChannelColor3(String channelColor3) {
        this.channelColor3 = channelColor3;
    }

    public String getChannelCategory() {
        return channelCategory;
    }

    public void setChannelCategory(String channelCategory) {
        this.channelCategory = channelCategory;
    }

    public String getChannelStbNumber() {
        return channelStbNumber;
    }

    public void setChannelStbNumber(String channelStbNumber) {
        this.channelStbNumber = channelStbNumber;
    }

    public boolean getChannelHD() {
        return channelHD;
    }

    public void setChannelHD(boolean channelHD) {
        this.channelHD = channelHD;
    }

    public int getHdSimulcastChannel() {
        return hdSimulcastChannel;
    }

    public void setHdSimulcastChannel(int hdSimulcastChannel) {
        this.hdSimulcastChannel = hdSimulcastChannel;
    }

    public String getChannelStartDate() {
        return channelStartDate;
    }

    public void setChannelStartDate(String channelStartDate) {
        this.channelStartDate = channelStartDate;
    }

    public String getChannelEndDate() {
        return channelEndDate;
    }

    public void setChannelEndDate(String channelEndDate) {
        this.channelEndDate = channelEndDate;
    }

    public List<ChannelExtRef> getChannelExtRef() {
        return channelExtRef;
    }

    public void setChannelExtRef(List<ChannelExtRef> channelExtRef) {
        this.channelExtRef = channelExtRef;
    }

    public List<LinearOttMapping> getLinearOttMapping() {
        return linearOttMapping;
    }

    public void setLinearOttMapping(List<LinearOttMapping> linearOttMapping) {
        this.linearOttMapping = linearOttMapping;
    }
}
