package sample.astro.com.features.getchannels.usecase;


import javax.inject.Inject;

import io.reactivex.Observable;
import sample.astro.com.application.helper.RealmHelper;
import sample.astro.com.base.usecase.BaseUseCase;
import sample.astro.com.features.getchannels.api.repository.GetChannelsRepository;
import sample.astro.com.features.getchannels.model.GetChannelListResult;
import sample.astro.com.features.getchannels.model.GetChannelsResult;

public class GetChannelsUseCaseImpl extends BaseUseCase implements GetChannelsUseCase {

    private final GetChannelsRepository repository;
    private final RealmHelper realmHelper;

    @Inject
    public GetChannelsUseCaseImpl(GetChannelsRepository repository, RealmHelper realmHelper) {
        this.repository = repository;
        this.realmHelper = realmHelper;
    }

    @Override
    public Observable<GetChannelListResult> getChannelList() {
        return request(repository.getChannelList())
                .map(response -> GetChannelListResult.transform(response, realmHelper));
    }

    @Override
    public Observable<GetChannelsResult> getChannels() {
        return request(repository.getChannels())
                .map(response -> GetChannelsResult.transform(response, realmHelper));
    }
}
