package sample.astro.com.features.tvguide.api.model.searchevent;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import sample.astro.com.base.model.api.BaseResponse;



public class SearchEventResponse extends BaseResponse {

    @SerializedName("hitsFound")
    private int hitsFound;
    @SerializedName("events")
    private List<Events> events;

    public int getHitsFound() {
        return hitsFound;
    }

    public void setHitsFound(int hitsFound) {
        this.hitsFound = hitsFound;
    }

    public List<Events> getEvents() {
        return events;
    }

    public void setEvents(List<Events> events) {
        this.events = events;
    }
}
