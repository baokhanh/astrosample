package sample.astro.com.features.getchannels.injection;


import sample.astro.com.application.injection.AppComponent;
import sample.astro.com.application.injection.quanlifier.PerFragment;
import sample.astro.com.features.getchannels.view.GetChannelsFragment;

import dagger.Component;

@PerFragment
@Component(dependencies = AppComponent.class, modules = {GetChannelsModule.class})
public interface GetChannelsComponent {
    void inject(GetChannelsFragment fragment);
}
