package sample.astro.com.features.getchannels.presenter;

import android.support.annotation.NonNull;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.observers.DisposableObserver;
import sample.astro.com.application.helper.RealmHelper;
import sample.astro.com.application.prefs.AstroPreference;
import sample.astro.com.base.usecase.BaseSubscribe;
import sample.astro.com.features.getchannels.model.ChannelInfo;
import sample.astro.com.features.getchannels.model.GetChannelListResult;
import sample.astro.com.features.getchannels.model.GetChannelsResult;
import sample.astro.com.features.getchannels.usecase.GetChannelsUseCase;
import sample.astro.com.features.getchannels.view.GetChannelsView;

public class GetChannelsPresenterImpl extends BaseChannelPresenterImpl<GetChannelsView> implements GetChannelsPresenter {

    private GetChannelsUseCase useCase;
    private ChannelInfo channelInfoWaitingAddFavorite;

    @Inject
    public GetChannelsPresenterImpl(GetChannelsView view, AstroPreference appPreference, GetChannelsUseCase useCase, RealmHelper realmHelper) {
        super(view, appPreference, realmHelper);
        this.useCase = useCase;
    }

    @Override
    public void loadChannelList() {
        // check db first: exist? display them and load from server to update later
        if (realmHelper.isHasObject(ChannelInfo.class)) {
            List<ChannelInfo> channelInfos = realmHelper.getChannelList();
            notifyDisplayChannelList(channelInfos);
            getChannelsFromServer();
        } else {
            // list not exist in db, load list sample first.
            getChannelListFromServer();
        }
    }

    @Override
    public void getChannelListFromServer() {
        Observable<GetChannelListResult> observable = useCase.getChannelList();
        DisposableObserver<GetChannelListResult> disposableObserver = new BaseSubscribe<GetChannelListResult>(view) {
            @Override
            public void onNext(GetChannelListResult result) {
                notifyDisplayChannelList(result.channels);
                saveListChannelToDb(result.channels);
                // continue load list detail.
                getChannelsFromServer();
            }

            @Override
            public void onError(@NonNull Throwable e) {
                super.onError(e);
                view.showProgressDialog(false);
            }
        };
        addToDisposable(disposableObserver);
        observable.subscribe(disposableObserver);
    }

    @Override
    public void getChannelsFromServer() {
        Observable<GetChannelsResult> observable = useCase.getChannels();
        DisposableObserver<GetChannelsResult> disposableObserver = new BaseSubscribe<GetChannelsResult>(false, view) {
            @Override
            public void onNext(GetChannelsResult result) {
                notifyDisplayChannelList(result.channels);
                saveListChannelToDb(result.channels);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                super.onError(e);
                view.showProgressDialog(false);
            }
        };
        addToDisposable(disposableObserver);
        observable.subscribe(disposableObserver);
    }

    private void notifyDisplayChannelList(List<ChannelInfo> channels) {
        sortChannelName(channels);
        view.updateListChannel(channels);
        view.displayListChannel();
    }

    @Override
    public void addChannelToFavorite(ChannelInfo info) {
        if (appPreference.isUserLoggedIn()) {
            realmHelper.addChannelToFavorite(info);
            //add to fire base
            addToFireBase(info);
        } else {
            channelInfoWaitingAddFavorite = info;
            goToLoginScreen();
        }
    }

    @Override
    public void goToLoginScreen() {
        view.goToLoginScreen();
    }

    @Override
    public void addChannelToFavoriteAfterLogin() {
        addChannelToFavorite(channelInfoWaitingAddFavorite);
    }
}