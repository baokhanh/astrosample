package sample.astro.com.features.getchannels.api.repository;

import io.reactivex.Observable;
import sample.astro.com.features.getchannels.api.model.GetChannelListResponse;
import sample.astro.com.features.getchannels.api.model.GetChannelsResponse;

public interface GetChannelsRepository {

    Observable<GetChannelListResponse> getChannelList();

    Observable<GetChannelsResponse> getChannels();
}
