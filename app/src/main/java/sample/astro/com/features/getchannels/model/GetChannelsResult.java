package sample.astro.com.features.getchannels.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import sample.astro.com.application.helper.RealmHelper;
import sample.astro.com.base.model.BaseUiInfo;
import sample.astro.com.features.getchannels.api.model.Channel;
import sample.astro.com.features.getchannels.api.model.GetChannelsResponse;

public class GetChannelsResult extends BaseUiInfo implements Serializable {
    public List<ChannelInfo> channels;

    public static GetChannelsResult transform(GetChannelsResponse response, RealmHelper realmHelper) {
        GetChannelsResult model = new GetChannelsResult();
        model.errorCode = response.getErrorCode();
        model.errorMessage = response.getErrorMessage();
        List<Channel> channels = response.getChannels();
        List<ChannelInfo> channelListInfos = new ArrayList<>();
        Set<Integer> channelIds = realmHelper.getFavoriteListId();
        if (channels != null && !channels.isEmpty()) {
            for (Channel info : channels) {
                channelListInfos.add(ChannelInfo.transform(info, channelIds.contains(info.getChannelId())));
            }
        }
        model.channels = channelListInfos;
        return model;
    }
}
