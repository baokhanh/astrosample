package sample.astro.com.features.getchannels.api.model;

import com.google.gson.annotations.SerializedName;

public class LinearOttMapping {
    @SerializedName("platform")
    private String platform;

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }
}
