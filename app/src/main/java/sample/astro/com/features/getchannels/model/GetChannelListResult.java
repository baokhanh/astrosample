package sample.astro.com.features.getchannels.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import sample.astro.com.application.helper.RealmHelper;
import sample.astro.com.base.model.BaseUiInfo;
import sample.astro.com.features.getchannels.api.model.Channel;
import sample.astro.com.features.getchannels.api.model.GetChannelListResponse;

public class GetChannelListResult extends BaseUiInfo implements Serializable {
    public List<ChannelInfo> channels;

    public static GetChannelListResult transform(GetChannelListResponse response, RealmHelper realmHelper) {
        GetChannelListResult model = new GetChannelListResult();
        model.errorCode = response.getErrorCode();
        model.errorMessage = response.getErrorMessage();
        List<Channel> channels = response.getChannels();
        List<ChannelInfo> channelListInfos = new ArrayList<>();
        if (channels != null && !channels.isEmpty()) {
            for (Channel info : channels) {
                channelListInfos.add(ChannelInfo.transform(info, false));
            }
        }
        model.channels = channelListInfos;

        return model;
    }
}
