package sample.astro.com.features.tvguide.api.model.getevent;

import com.google.gson.annotations.SerializedName;

public class ContentImage {
    @SerializedName("imageURL")
    private String imageURL;
    @SerializedName("imageRole")
    private String imageRole;

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getImageRole() {
        return imageRole;
    }

    public void setImageRole(String imageRole) {
        this.imageRole = imageRole;
    }
}
